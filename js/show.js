/*
 *      show.js
 *       
 *      Copyright 2011 Mario Rubiales <mariorubiales@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function do_show_form_newplayer(){
	$("#record div.content").hide();
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=5",
		success:function(data) {
			$("#record div.content").html(data);
			$("#record div.content").show();
			do_make_links_newplayer();
		}
	});
}

function do_make_links_newplayer(){
	$("#btns_record input.accept").click(function (){
		
		if ($("#record div.content #dnewplayer input#nameplayer").val()==""){
			alert("Maaaaal, se te olvida el nombre del jugador");
			return false;
		}

		var text = $("#record div.content #dnewplayer input#nameplayer").val();
		var regex = /^[A-Z]/;
		if (! regex.exec(text)){
			alert("La primera con mayúsculas por favor !!");
			return false;
		}	

		if ($("#record div.content #dnewplayer select#selectdem").val()==0){
			alert("Maaaaal, se te olvida la demarcación");
			return false;
		}	
		
		if ($("#record div.content #dnewplayer select#selectlfpteam").val()=="XXX"){
			alert("Maaaaal, se te olvida el equipo");
			return false;
		}
		do_set_newplayer();
	});
	
	$("#btns_record input.cancel").click(function (){
		$("#record div.content").hide();
	});
}

function do_set_newplayer(){
	do_show_loading("#record #btns_record");
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=6&"+$("#record div.content #dnewplayer #form_new_player").serialize(),
		success:function(xml) {
			setTimeout(function(){
					do_show_record_player($(xml).find('newplayer').text(),modify=0);
					do_update_log(xml);
					do_reload_autocomplete_list();
			},1000);
		}
	});
}

function do_reload_autocomplete_list(){
	$.ajax({
	  url: "includes/js.php",
	  data: "opt=2",
	  success: function (data){
	  	jQuery.globalEval(data);
	  	$("#playername").autocomplete({
				source: players,
				select: function(event, ui) {
        	do_show_record_player(ui.item.id,0);
        }
			});
	  }
	});
}

function do_show_record_player(idplayer,modify){
	do_show_loading($("#record div.content"));
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=1&idplayer="+idplayer+"&modify="+modify,
		success:function(data) {
			setTimeout(function(){
				$("#record div.content").html(data);
				$("#record div.content").show();
				do_make_links_record();
				do_make_link_modify(idplayer);
			},400);
		}
	});
	
}


function do_show_record_player_ro(idplayer){
	do_show_loading($("#record div.content"));
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=8&idplayer="+idplayer,
		success:function(data) {
			setTimeout(function(){
				$("#record div.label").html(":: datos jugador ::");
				$("#record div.content").html(data);
				$("#record div.content").show();
				$("#btns_record input.cancel").bind("click",function (){
					do_launch_graph();
				});
			},500);	
		}
	});
	
}

function do_make_links_record(){
	$("#btns_record input.accept").bind("click",function (){
		//El coste solo número
		var regex_int=/^\d+$/;
		
		if ( ($("#record div.content input#oldprop").val()==0) && ($("#record div.content select#selectteam").val()==0) ){
			alert("Quién ficha a este jugador ??");
			return false;
		}	
		
		if (!$("#record div.content input#cost").val().match(regex_int)){
				alert("Revisa el coste del jugador !!!");
				return false;
		}
		
		//Antes de enviar nada desactivamos los campos para evitar tonterias
		$("#record div.content input#cost").attr("disabled", true);
		$("#record div.content select#selectteam").attr("disabled", true);
			
		do_set_transfer($("#record div.content input#idplayer").val(),$("#record div.content input#oldprop").val(),
		$("#record div.content select#selectteam").val(),$("#record div.content input#cost").val(),
		$("#record div.content input#oldcost").val(),$("#record div.content input#iddem").val());
  });
  
  $("#btns_record input.cancel").bind("click",function (){
  	$("#record div.content").hide();
  });
}

function do_set_transfer(idplayer,oldprop,newprop,cost,oldcost,dem){
	do_show_loading("#record #btns_record");
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=2&idplayer="+idplayer+"&idoldprop="+oldprop+"&idnewprop="+newprop+"&cost="+cost+"&oldcost="+oldcost+"&dem="+dem,
		success:function(xml) {
			/* Creamos un retardo ficticio de un segundo para que el usuario tenga conciencia de que la operación
			 * se ha realizado, al ser una aplicación que funciona sobre un red local a veces las operaciones se hacen en décimas de 
			 * segundo y el usuario puede que no se de cuenta de como han ocurrido. Es recomendable quitar este y otros retardos si 
			 * alguna vez la aplicación se publica en internet
			 * */
			setTimeout(function(){
				if ($(xml).find('error').attr("id")=="0"){ //si no hay error seguimos adelante
					var info=$(xml).find('info').text();
					do_show_record_player(idplayer,modify=0);
					do_update_summaryandlog(xml);
					$("#searcher .content #inputsearch #playername").val("");
					if (info!="") alert(info);
				}
				else{
					var htmlerror="<div class='msgerror'><p>"+$(xml).find('error').text()+"</p></div>";
					$("#record div.content").html(htmlerror);
					$("#record div.content").show();	
				}	
			},650);
		}
	});
	
}

function get_just_summary(idteam){
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=15&idteam="+idteam,
		success:function(xml) {
			/* Creamos un retardo ficticio de un segundo para que el usuario tenga conciencia de que la operación
			 * se ha realizado, al ser una aplicación que funciona sobre un red local a veces las operaciones se hacen en décimas de 
			 * segundo y el usuario puede que no se de cuenta de como han ocurrido. Es recomendable quitar este y otros retardos si 
			 * alguna vez la aplicación se publica en internet
			 * */
			setTimeout(function(){
				do_update_just_summary(xml);
			},650);
		}
	});
}

function do_update_just_summary(xml){
	$(xml).find('summary').each(function(){
		var id=$(this).attr('id');
		var gastado=$(this).find('gastado').text();
		var presupuesto=$(this).find('presupuesto').text();
		var resto=$(this).find('resto').text();
		var por=$(this).find('por').text();
		var def=$(this).find('def').text();	
		var med=$(this).find('med').text();	
		var del=$(this).find('del').text();
		var total=$(this).find('total').text();
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.spresupuesto"),presupuesto);
		if ($("#summary div.content table#ideq_"+id+" tr td.sresto").length > 0)
			do_show_change_rest($("#summary div.content table#ideq_"+id+" tr td.sresto"),resto);
		else
			do_show_change_rest($("#summary div.content table#ideq_"+id+" tr td.srestow"),resto);
		do_show_change_rest($("#summary div.content table#ideq_"+id+" tr td.sresto"),resto);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.sgastado"),gastado);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.spor"),por);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.sdef"),def);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.smed"),med);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.sdel"),del);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.stotal"),total);
	});
}

function do_update_summaryandlog(xml){
	$(xml).find('summary').each(function(){
		var id=$(this).attr('id');
		var gastado=$(this).find('gastado').text();
		var resto=$(this).find('resto').text();
		var por=$(this).find('por').text();
		var def=$(this).find('def').text();	
		var med=$(this).find('med').text();	
		var del=$(this).find('del').text();
		var total=$(this).find('total').text();
		if ($("#summary div.content table#ideq_"+id+" tr td.sresto").length > 0)
			do_show_change_rest($("#summary div.content table#ideq_"+id+" tr td.sresto"),resto);
		else
			do_show_change_rest($("#summary div.content table#ideq_"+id+" tr td.srestow"),resto);
		do_show_change_rest($("#summary div.content table#ideq_"+id+" tr td.sresto"),resto);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.sgastado"),gastado);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.spor"),por);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.sdef"),def);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.smed"),med);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.sdel"),del);
		do_show_change($("#summary div.content table#ideq_"+id+" tr td.stotal"),total);
	});

	$(xml).find('log').each(function(){
		var fecha=$(this).find('fecha').text();
		var tipo=$(this).find('tipo').text();	
		var coste=$(this).find('coste').text();
		var team=$(this).find('team').text();	
		var player=$(this).find('player').text();	
		var lastcount=$("#main #labelcount #count").text();
		lastcount++;
		$("#main #labelcount #count").text(lastcount);
		var html="<tr><td title='"+tipo+"'>"+fecha+"</td><td>"+team+"</td><td>"+player+"</td><td>"+coste+"</td></tr>";
		$("#log div.content table tr#headlog").after(html);
		check_do_backup(lastcount,rotatebck);
	});
	
	$(xml).find('formation').each(function(){
		var extra=$(this).find('extra').text();
		var team=$(this).find('team').text();
		var idteam=$(this).find('idteam').text();
		var playername=$(this).find('playername').text();	
		alert(team+" se ha embolsado "+extra+ " kilos en concepto de derechos de formación por la venta de "+playername);
		get_just_summary(idteam);
	});
	
}

function do_update_log(xml){
	$(xml).find('log').each(function(){
		var fecha=$(this).find('fecha').text();
		var tipo=$(this).find('tipo').text();	
		var coste=$(this).find('coste').text();
		var team=$(this).find('team').text();	
		var player=$(this).find('player').text();	
		var lastcount=$("#main #labelcount #count").text();
		lastcount++;
		$("#main #labelcount #count").text(lastcount);
		var html="<tr><td title='"+tipo+"'>"+fecha+"</td><td>"+team+"</td><td>"+player+"</td><td>"+coste+"</td></tr>";
		$("#log div.content table tr#headlog").after(html);
		check_do_backup(lastcount,rotatebck);
	});
}

function do_make_link_modify(idplayer){
	//Ya que jQuery siempre retorna un objeto al utilizar los selectores esta es la forma de saber si un elemento del DOM existe o no, comprobando su longuitud, 
	if ($("#record #btns_record input#btnmod").length > 0){ //Si el botón existe asignamos la acción
		$("#record #btns_record input#btnmod").bind("click",function (){
	  	do_show_record_player(idplayer,modify=1);
	  });
 	}
}

function do_show_players4teams_ro(element){
	var cod=$(element).attr("id");
	$("#players4teams .content").hide();
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=3&codteam="+cod,
		success:function(data) {
			/* ponemos un poco de retardo al mostrar la información para que el loader se vea y así el usuario
			 * no tenga dudas de que la información se está actualizando. Al estar la aplicación alojada en la misma red local
			 * todo ocurre muy rápido y a veces parece que no ha pasado nada.
			 * */
			setTimeout(function (){
				$("#players4teams .content").html(data);
				$("#players4teams .content").show();
				$("#lfpteams .content table tr td").removeClass("teamselected");
				$("#lfpteams .content table tr td#"+cod).addClass("teamselected");
				$("#players4teams .content table tr td.tdplayername").bind("click",function (){
					do_show_record_player_ro($(this).attr("id"));
				});
				$(element).css("background-image","url('images/vinetateams.png')");
			},500); 
			
		}
	});
}

function do_show_players4teams(element){
	var cod=$(element).attr("id");
	$("#players4teams .content").hide();
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=3&codteam="+cod,
		success:function(data) {
			/* ponemos un poco de retardo al mostrar la información para que el loader se vea y así el usuario
			 * no tiene dudas de que la información se está actualizando. Al estar la aplicación alojada en la misma red local
			 * a veces parece que no ha ocurrido nada.
			 * */
			setTimeout(function (){
				$("#players4teams .content").html(data);
				$("#players4teams .content").show();
				$("#lfpteams .content table tr td").removeClass("teamselected");
				$("#lfpteams .content table tr td#"+cod).addClass("teamselected");
				do_make_links_players4teams();
				$(element).css("background-image","url('images/vinetateams.png')");
			},500);
		}
	});
}

function do_make_links_players4teams(){
	$("#players4teams .content table tr td.tdplayername").bind("click",function (){
  	do_show_record_player($(this).attr("id"),modify=0);
  });
}

function do_show_summary(idequipo){
	$("#report_popup").css("visibility","visible");
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=7&idequipo="+idequipo,
		success:function(data) {
			$("#report_popup").html(data);
			$("#report_popup").show();
			$("#report_popup #close_report img#close").click(function (){
				$("#report_popup").hide();
			})
		}
	});
}

function do_get_full_log(order){
	do_show_loading ("#main #log div.content");
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=10&order="+order,
		success:function(data) {
			setTimeout(function(){
				$("#main #log div.content").html(data);
				$("#main #log div.content").show();
				$("#log div.content table tr th.sortable").click(function (){
					do_get_full_log($(this).attr("id"));
				});
			},1000);
		}
	});
}

function do_get_count_transfers(){
	$("#main #labelcount #count").text("...");
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=13",
		success:function(data) {
			setTimeout(function(){
				$("#main #labelcount #count").text(data);
			},1000);
		}
	});
}

function do_get_full_summary(){
	do_show_loading ("#main #working #summary div.content");
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=11",
		success:function(data) {
			setTimeout(function(){
				$("#main #working #summary div.content").html(data);
				$("#main #working #summary div.content").show();
				//generamos de nuevo el evento click para el resumen
				$("#summary .content .tsummary tr th.headteam").click(function (){
			  	var videquipo = new Array(); 
					videquipo = $(this).attr("id").split("_");
			  	do_show_summary(videquipo[1]);
			  });
			},1000);
		}
	});
}
