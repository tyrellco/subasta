/*
 *      m.show.js
 *       
 *      Copyright 2014 Mario Rubiales <mariorubiales@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function do_get_global_summary(){
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=100",
		success:function(data) {
			$.mobile.changePage("#global_summary",{role:"page"});
			$("div#content_global_summary").html(data);
			$('div#content_global_summary table').table().trigger('create');
		}
	});
}

function do_get_summary_mobile(){
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=101",
		success:function(data) {
			$.mobile.changePage("#summary",{role:"page"});
			$("div#content_summary").html(data);
			$('div.summary_team').collapsible().trigger('create');
			$('div#content_summary').collapsibleset().trigger('create');
			$("div.summary_team a.report_team").bind("click",function (){
					$.mobile.changePage("#reportteam",{role:"page"});
					do_get_reportteam($(this).attr("id"));
			});
		}
	});
}

function do_get_infoplayer(id){
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: {opt: 103,idplayer:id},
		success:function(data) {
			$("#content_infoplayer").html(data);
			$('div#content_infoplayer ul').listview().trigger('create');
		}
	});
}

function do_get_reportteam(id){
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: {opt: 104,idteam:id},
		success:function(data) {
			$("#content_reportteam").html(data);
			$('div#content_reportteam table').table().trigger('create');
		}
	});
}

function do_get_lfpteams_mobile(){
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=102",
		success:function(data) {
			$.mobile.changePage("#lfpteams",{role:"page"});
			$("div#content_lfpteams").html(data);
			$('div.summary_lfpteam').collapsible().trigger('create');
			$('div#content_lfpteams').collapsibleset().trigger('create');
			//creamos el click para cada jugador
			$("li a.infoplayer").bind("click",function (){
					//lanzamos la página con informacion del jugador en modo diago mediante una instrucción de jquery mobile
					$.mobile.changePage("#infoplayer",{role:"dialog"});
					do_get_infoplayer($(this).attr("id"));
			});
		}
	});
}

function do_get_log_mobile(){
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=105",
		success:function(data) {
			$.mobile.changePage("#log",{role:"page"});
			$("div#content_log").html(data);
			$('div#content_log table').table().trigger('create');
		}
	});
}

