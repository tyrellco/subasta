/*
 *      global.js
 *       
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


//Esto es lo primero que se ejecuta nada más cargar la página.

function init_app(){

	$("#playername").autocomplete({
				source: players,
				select: function(event, ui) {
	    	do_show_record_player(ui.item.id,modify=0);
	    }
	});

	$("#playername_ro").autocomplete({
	 			source: players,
				select: function(event, ui) {
	    	do_show_record_player_ro(ui.item.id);
	    }
	});

	$("#lfpteams .content table tr td").bind("click",function (){
		$(this).css("background-image","url('images/ajax-loader.gif')");
		do_show_players4teams($(this));
	});

	$("#lfpteams .content table tr td.desktop").bind("click",function (){
		do_show_players4teams_ro($(this));
	});

	$("#infouser ul li#username").click(function (){
		var html="<input type='text' id='newsubastador' name='newsubastador' value='"+$("#infouser ul li#username").text()+"'/>";
		$("#infouser ul li#username").html(html);
		$("#infouser ul li#username input").focus();
		$("#infouser ul li#username input").keyup (function (event){
			if (event.keyCode === 13) {
				do_set_new_user($("#infouser ul li#username input").val());
		}
		});
		$("#infouser ul li#username input").blur(function (){
			do_set_new_user($("#infouser ul li#username input").val());
		});
	});

	$("#infouser ul li#logout").click(function (){
		do_exit();
	});

	$("#infouser ul.nonetbook li#reload").click(function (){
		do_get_full_log(0);
		do_get_count_transfers();		
		do_get_full_summary();
		do_reload_graph();
	});

	$("#infouser ul.netbook li#reload").click(function (){
		do_get_full_summary();
		do_reload_graph();
	});

	$("#infouser ul li#back").click(function (){
		window.location.href="index.php";
	});

	$("#log div.content table tr th.sortable").click(function (){
		do_get_full_log($(this).attr("id"));
	});
	  
	$("#footer #exports span.exportpdf").click(function (){
		window.open("includes/reports.inc.php?opt=2");  		
	});

	$("#footer #exports span.exportcsv").click(function (){
		window.open("includes/reports.inc.php?opt=3");  		
	});

	/*$("#footer #flavors span.switchyear").click(function (){
			$.ajax({
			type: 'POST',
			url: "ajax/ajaxsite.php",
			data: "opt=14&year="+$("#footer #flavors span.switchyear").attr("id"),
			success:function(data) {
				location.reload();
			}
		});
		alert("Lo haré cuando tenga tiempo ....");
	});*/

	$("#main #working #summary div.label").click(function (){
		window.open("includes/reports.inc.php?opt=2");
	});

	$("#main #log div.label").click(function (){
		window.open("includes/reports.inc.php?opt=3");
	});

	$("#searcher #inputnew input#btnnew").click(function (){
		do_show_form_newplayer();	
	});

	$("#summary .content .tsummary tr th.headteam").click(function (){
		var videquipo = new Array(); 
		videquipo = $(this).attr("id").split("_");
		do_show_summary(videquipo[1]);
	});
	
}

function do_exit(){
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=20",
		success:function(data) {
			window.location.href="index.php";
		}
	});
}

function do_show_loading(idlayer){

var htmlvalue="<div class='loading'>cargando ...</div>";
$(idlayer).html(htmlvalue);
	
}

function do_show_change(element,value){
	$(element).hide();
	$(element).text(value).fadeIn(800);
}

function do_show_change_rest(element,value){
	$(element).hide();
	if (value < poor){
		$(element).removeClass("sresto");
		$(element).addClass("srestow");
	}	
	else{	
		$(element).removeClass("srestow");
		$(element).addClass("sresto");
	}	
	$(element).text(value).fadeIn(800);
}

function do_set_new_user(username){
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=4&username="+username,
		success:function(data){
			$("#infouser ul li#username").html(username);
			$("#infouser ul li#last").html(data);
		}
	});
	
}

function do_launch_graph(){
	var preparehtml="<div id='graph'></div>";
	$("#record div.content").html(preparehtml);
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=9",
		success:function(xml){
			draw_graph(xml);
			$("#record div.label").html(":: gr&aacute;fica dinero restante ::");
		}
	});
}

function do_reload_graph(){
	do_show_loading("#main #working #record div.content #graph");
	var preparehtml="<div id='graph'></div>";
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=9",
		success:function(xml){
			setTimeout(function(){
				$("#record div.content").html(preparehtml);
				$("#record div.label").html(":: gr&aacute;fica dinero restante ::");
				draw_graph(xml);
			},1000);
		}
	});
}

function draw_graph(xml){
	//var sausage_names="";
	var s1 = new Array();
	var ticks = new Array();
	var cont=0;

	$(xml).find('gastado').each(function(){
		s1[cont]=parseInt($(this).text());
		ticks[cont]=$(this).attr('id');
		cont ++;
	});
	
	/*var s1 = [100,2500,3000,400,1500,600,700,2800,900,20,611,710,1300,2140];
	var ticks = ["Viejo toni", "Vidal", "santi", "pepe","Mario","julian","Juanma","Juan","Josemi","Javi","isa","Fernando","Carlos","Alberto"];*/
	plot1 = $.jqplot('graph', [s1], {
			seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					pointLabels: { show: true },
					color: "#FF247A", //color barras
			},
			axes: {
				xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						ticks: ticks,
				}
			},
			grid: {
			  background: '#FFFFFF',// CSS color spec for background color of grid.
			  shadow: false,
   		 },
			highlighter: { show: false }
	});
}

function check_do_backup(lastcount,rotatebck){
	if (lastcount%rotatebck==0) //hacemos backup
		//do_backup();
		alert("Se han hecho "+lastcount+" fichajes, avisa a Mario para que haga una copia de la BBDD");
}

function do_backup(){
	$("#bck_popup").css("visibility","visible");
	$.ajax({
		type: 'POST',
		url: "ajax/ajaxsite.php",
		data: "opt=12",
		success:function(data){
			setTimeout(function(){
				$("#bck_popup").css("visibility","hidden");
			},3000);
		}
	});
}

function adjust_resolution(){
	if (screen.width<1280){
		$("#container").css("left","27px");
		$("#header #logo").css("left","358px");
		$("#footer").css("left","30px");
	}	
}
