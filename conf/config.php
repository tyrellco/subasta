<?php
/*
 *      config.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

/***************** BBDD *****************/

//MySQL
define ("SERVER","172.18.0.2"); //docker inspect mariadb_default -> para saber la ip del docker de MySQL
define ("BBDD","subasta");
define ("USER","subasta");
define ("PASSWD","antiliga11");

/**************PARÁMETROS SUBASTA***************/
// jugadores máximos en una plantilla
define("MAXNUMBER_PLAYERS",22);
// jugadores mínimos en una plantilla
define("MINNUMBER_PLAYERS",15);
// precio mínimo a pagar por un jugador
define("MINPRICE",20);
/*******PARÁMETROS DERECHO DE FORMACIÓN********/
// porcentaje del derecho de formación, 20%
define ("PERCENTFORMAC",20);
// cantidad fija por derecho de formación
define ("EXTRAFORMAC",50);
// cantidad máxima que se obtendrá por jugador en derecho de formación
define ("MAXEXTRA",200);

/***************** GENERAL *****************/
//prefijo de la BBDD para la temporada actual
define ("PREF",0);
//aplicacion sólo lectura (0:NO;1:SI)
define("READONLY",0);
//numero de movimientos tras los cuales se hace un backup de la BBDD
define("ROTATEBCK",50);
//hoja de estilos por defecto
define("DEFAULTCSS","day");
// resto de dinero por debajo del cual se muestra el cuadro resumen (RESTO) en rojo
define ("POOR",500);
//locales
setlocale(LC_TIME, 'es_ES');

/***************** ABOUT *****************/
define ("YEAR","2021");
define("APPNAME","subasta-antiliga");
define("APPVERSION","v1.0");

/******************PDF INFO**************************/
define("PDFPRODUCER",APPNAME." ".APPVERSION);
//Asunto
define("PDFSUBJECT",utf8_decode("Informe de ".APPNAME. "año ".YEAR));


?>
