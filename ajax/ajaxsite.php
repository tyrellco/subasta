<?php
/*
 *      ajaxsite.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/../tmp");
session_start();
require_once (dirname(__FILE__)."/../includes/generator.inc.php");
require_once (dirname(__FILE__)."/../includes/m.generator.inc.php");

switch ($_POST["opt"]){
	case 1: echo get_record_player($_POST["idplayer"],$_POST["modify"]);
					break;
	case 2: echo set_transfer($_POST["idplayer"],$_POST["idoldprop"],$_POST["idnewprop"],$_POST["cost"],$_POST["oldcost"],$_POST["dem"]);
					break;
	case 3: echo get_players4teams($_POST["codteam"]);
					break;
	case 4: echo set_new_user($_POST["username"]);
					break;
	case 5: echo get_form_newplayer();
					break;	
	case 6: echo set_new_player($_POST);
					break;
	case 7: echo get_report($_POST["idequipo"]);
					echo get_summary($_POST["idequipo"]);
					break;
	case 8: echo get_record_player_readonly($_POST["idplayer"]);
					break;
	case 9: echo get_xml_graph();
					break;
	case 10: echo get_full_log($_POST["order"]);
					 break;
	case 11: echo get_full_summary();
					 break;
	case 12: echo make_backup();
					 break;
	case 13: echo get_count_transfers();
					 break;
	case 14: echo set_year($_POST["year"]);
					 break;	
	case 15: echo get_just_xml_summary($_POST["idteam"]);
					 break;
//version mobile
	case 100: echo get_global_summary();
						break;						
	case 101: echo get_full_summary_collapsible();
						break;
	case 102: echo get_lfpteams_collapsible();
						break;
	case 103: echo get_info_player($_POST["idplayer"]);
						break;
	case 104: echo get_report_team($_POST["idteam"]);
						break;
	case 105: echo get_full_log_mobile();
						break;
//					
	case 20: echo byebye();
					 break;
}

?>
