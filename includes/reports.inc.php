<?php
/*
 *      file.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/../tmp");
session_start();

require_once(dirname(__FILE__)."/../classes/BBDD.class.php");
require_once (dirname(__FILE__)."/../lib/pdf-php/class.ezpdf.php");


$pdf =new Cezpdf("a4");
// $pdf = new UTF8_Cezpdf('a4','portrait');
$pdf->selectFont(dirname(__FILE__)."/../lib/pdf-php/fonts/Helvetica.afm");
$pdf->ezSetCmMargins(1.5,1.5,1.5,1.5);

	
switch ($_GET["opt"]){
	case 1: get_report_team_pdf($_GET["idequipo"]);
					break;
	case 2: get_report_allteam_pdf();
					break;
	case 3: log2cvs();
					break;													
}	

/*************FUNCIONES GENERICAS NECESARIAS PARA LA CREACIÓN DEL PDF*****************/

//Funcion que hace lo mismo que get_pref de global.inc.php. Algo hay en este fichero que genera un warning header, para salir del paso creo esta funcion local
function get_pref_reports(){
	if (isset($_SESSION["year"]))
		return $_SESSION["year"];
	else
		return PREF;	
}

function get_fecha_actual(){
	return date("d\/m\/Y\ -\ H:i:s");
}

function set_pdf_info($pdf,$title){
	$pdf->addInfo("Title",$title);
	$pdf->addInfo("Author","subasta-antiliga");
	$pdf->addInfo("Producer",PDFPRODUCER);
	$pdf->addInfo("Subject",PDFSUBJECT);
	$pdf->addInfo("CreationDate",get_fecha_actual());
	return $pdf;
}

function set_header_footer($pdf){
	$pdf->setLineStyle(1);			
	$all = $pdf->openObject();
	$pdf->setStrokeColor(0,0,0,1);
	$pdf->line(20,40,578,40);
	$momento=get_fecha_actual();
	$pdf->addText(20,33,7,$momento);
	$about=APPNAME." ".APPVERSION;
	$pdf->addText(505,33,7,$about);
	$pdf->closeObject();
	$pdf->addObject($all,'all');
	//$pdf->addPngFromFile('../images/fichado.png',495,760,50,0);
	//El logo sólo se incluye en la primera página
	//$pdf->addObject($all,'add');
	$pdf->ezStartPageNumbers(300,20,9,'','{PAGENUM} de {TOTALPAGENUM}',1); 
	
	return $pdf;
}

//Dibuja una línea en la posición y
function set_line($pdf,$y){
	$pdf->setLineStyle(1);			
	$all = $pdf->openObject();
	$pdf->setStrokeColor(0,0,0,1);
	$pdf->line(42,$y,542,$y);
	$pdf->closeObject();
	$pdf->addObject($all,'add');
	
	return $pdf;
}


function get_options_table(){
	$options = array(
					"showLines"=>1,
					"shadeCol"=>array(0.8,0.8,0.8) ,
					"fontSize"=>11,
					"titleFontSize"=>20,
					"xPos"=>"center",
					"xOrientation"=>"center",
					"rowGap"=>1,
					"width"=>500
				);
	return $options;			
}

function get_options_table_simple(){
	$options = array(
					"showLines"=>1,
					"shaded"=>0,
					"fontSize"=>11,
					"titleFontSize"=>20,
					"xPos"=>"center",
					"xOrientation"=>"center",
					"rowGap"=>1,
					"width"=>500
				);
	return $options;			
}

/*************************INFORMES DE LA SUBASTA*****************************************/

// Informe completo de un equipo en concreto
function get_report_team_pdf($idequipo){
	global $pdf;
	
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT jug.nombre,jug.demarcacion,jug.coste,jug.equipolfp,lfp.nombre lfpteam,eq.nombre nombreequipo FROM `".get_pref_reports()."_jugadores` jug INNER ";
	$sql .="JOIN `".get_pref_reports()."_equipos` eq ON (eq.id=jug.idprop) INNER JOIN `".get_pref_reports()."_equiposlfp` lfp ON (lfp.codigo=jug.equipolfp) WHERE jug.idprop=$idequipo ORDER BY jug.demarcacion,jug.nombre";
	$obj_players=$oBBDD->get_resource($sql);
	$count=1;
	$nameteam="";
	while ($player=mysqli_fetch_object($obj_players)){
		$nameteam=$player->nombreequipo;
		switch ($player->demarcacion) {
			case '1': $demtxt="Portero";
								$class="datapor";
			break;
			case '2': $demtxt="Defensa";
								$class="datadef";
			break;
			case '3': $demtxt="Medio";
								$class="datamed";
			break;
			case '4': $demtxt="Delantero";
								$class="datadel";
			break;
			default: $demtxt="Pendiente";
							 $class="";	
			break;
		}
		// Vamos generando la matriz asociativa para luego encasquetarsela al ezpdf
		$vdata[] = array("num"=>$count,"nombre"=>utf8_decode($player->nombre),"demarcacion"=>$demtxt,"lfp"=>$player->lfpteam,"coste"=>$player->coste);
		$count++;
	}	
	
	$subtitule=utf8_decode("subasta verano ".YEAR);
	$fields = array(
					"num"=>"<b>NUM</b>",
					"nombre"=>"<b>JUGADOR</b>",
					"demarcacion"=>"<b>DEMARCACION</b>",
					"lfp"=>"<b>EQUIPO LFP</b>",
					"coste"=>"<b>COSTE</b>"
				);
	$options=get_options_table();
	$pdf=set_pdf_info($pdf,$subtitule);
	$pdf= set_line($pdf,755);
	$pdf=set_header_footer($pdf);
	
	// Ahora sacamos los datos del resumen
	$sql="SELECT resum.idequipo idequipo, resum.presupuesto presupuesto, resum.gastado gastado, resum.porteros por, ";
	$sql .="resum.defensas def, resum.medios med, resum.delanteros del, equip.nombre nombre FROM `".get_pref_reports()."_resumen` ";
	$sql .="resum INNER JOIN `".get_pref_reports()."_equipos` equip ON (idequipo=equip.id) WHERE idequipo=$idequipo";
	$obj_summary=$oBBDD->get_resource($sql);
	$html="";
	$summary=mysqli_fetch_object($obj_summary);
	$resto=$summary->presupuesto - $summary->gastado;
	$total=$summary->por + $summary->def + $summary->med + $summary->del;
		
	$maintitle=$summary->nombre;
	$pdf->ezText("<b>".$maintitle."</b>", 36);
	$pdf->ezText("<b>".$subtitule."</b>\n\n", 10);
	$pdf->ezTable($vdata, $fields, "", $options);
	
	$options_simple=get_options_table_simple();

	$vsummary_detail[]=array("porteros"=>$summary->por,"defensas"=>$summary->def,"medios"=>$summary->med,"delanteros"=>$summary->del,"total"=>$total);
	$fields = array(
					"porteros"=>"<b>POR</b>",
					"defensas"=>"<b>DEF</b>",
					"medios"=>"<b>MED</b>",
					"delanteros"=>"<b>DEL</b>",
					"total"=>"<b>TOTAL</b>"
				);
	$pdf->ezText("\n",6);			
	$pdf->ezTable($vsummary_detail, $fields, "", $options_simple);
		
	$vsummary_money[]=array("presupuesto"=>$summary->presupuesto,"gastado"=>$summary->gastado,"resto"=>$resto);
	$fields = array(
					"presupuesto"=>"<b>PRESUPUESTO</b>",
					"gastado"=>"<b>GASTADO</b>",
					"resto"=>"<b>RESTO</b>"
				);
	$pdf->ezText("\n",6);	
	$pdf->ezTable($vsummary_money, $fields, "", $options_simple);
	
	$pdf->ezStream();
}

function get_report_allteam_pdf(){
	$title="SUBASTA ANTILIGA\n VERANO ".YEAR;
	$subtitule=utf8_decode("subasta verano ".YEAR);
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `id` FROM `".get_pref_reports()."_equipos` WHERE `id`<>0 ORDER BY `nombre`";
	$obj_teams=$oBBDD->get_resource($sql);
	$mainpdf =new Cezpdf("a4");
	$mainpdf->selectFont(dirname(__FILE__)."/../lib/pdf-php/fonts/Helvetica.afm");
	$mainpdf->ezSetCmMargins(1.5,1.5,1.5,1.5);
	$mainpdf=set_pdf_info($mainpdf,$subtitule);
	$mainpdf=set_header_footer($mainpdf);
	$mainpdf->ezText("\n",86);
	$mainpdf->ezText("<b>".$title."</b>\n\n", 40,array('justification'=>'center'));
	$mainpdf->ezNewPage();
	while ($teams=mysqli_fetch_object($obj_teams)){
		$mainpdf=get_objreport_team_pdf($mainpdf,$teams->id);
		$mainpdf->ezNewPage();
	}
	$mainpdf->ezStream();
}


function get_objreport_team_pdf($pdf,$idequipo){
		
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT jug.nombre,jug.demarcacion,jug.coste,jug.equipolfp,lfp.nombre lfpteam,eq.nombre nombreequipo FROM `".get_pref_reports()."_jugadores` jug INNER ";
	$sql .="JOIN `".get_pref_reports()."_equipos` eq ON (eq.id=jug.idprop) INNER JOIN `".get_pref_reports()."_equiposlfp` lfp ON (lfp.codigo=jug.equipolfp) WHERE jug.idprop=$idequipo ORDER BY jug.demarcacion,jug.nombre";
	$obj_players=$oBBDD->get_resource($sql);
	$count=1;
	$nameteam="";
	while ($player=mysqli_fetch_object($obj_players)){
		$nameteam=$player->nombreequipo;
		switch ($player->demarcacion) {
			case '1': $demtxt="Portero";
								$class="datapor";
			break;
			case '2': $demtxt="Defensa";
								$class="datadef";
			break;
			case '3': $demtxt="Medio";
								$class="datamed";
			break;
			case '4': $demtxt="Delantero";
								$class="datadel";
			break;
			default: $demtxt="Pendiente";
							 $class="";	
			break;
		}
		// Vamos generando la matriz asociativa para luego encasquetarsela al ezpdf
		$vdata[] = array("num"=>$count,"nombre"=>$player->nombre,"demarcacion"=>$demtxt,"lfp"=>$player->lfpteam,"coste"=>$player->coste);
		$count++;
	}	
	
	$subtitule=utf8_decode("subasta verano ".YEAR);
	$fields = array(
					"num"=>"<b>NUM</b>",
					"nombre"=>"<b>JUGADOR</b>",
					"demarcacion"=>"<b>DEMARCACION</b>",
					"lfp"=>"<b>EQUIPO LFP</b>",
					"coste"=>"<b>COSTE</b>"
				);
	$options=get_options_table();
	$pdf= set_line($pdf,755);
	
	// Ahora sacamos los datos del resumen
	$sql="SELECT resum.idequipo idequipo, resum.presupuesto presupuesto, resum.gastado gastado, resum.porteros por, ";
	$sql .="resum.defensas def, resum.medios med, resum.delanteros del, equip.nombre nombre FROM `".get_pref_reports()."_resumen` ";
	$sql .="resum INNER JOIN `".get_pref_reports()."_equipos` equip ON (idequipo=equip.id) WHERE idequipo=$idequipo";
	$obj_summary=$oBBDD->get_resource($sql);
	$html="";
	$summary=mysqli_fetch_object($obj_summary);
	$resto=$summary->presupuesto - $summary->gastado;
	$total=$summary->por + $summary->def + $summary->med + $summary->del;
	
	$maintitle=utf8_decode($summary->nombre);
	$pdf->ezText("<b>".$maintitle."</b>", 36);
	$pdf->ezText("<b>".$subtitule."</b>\n\n", 10);
	$pdf->ezTable($vdata, $fields, "", $options);
	
	$options_simple=get_options_table_simple();

	$vsummary_detail[]=array("porteros"=>$summary->por,"defensas"=>$summary->def,"medios"=>$summary->med,"delanteros"=>$summary->del,"total"=>$total);
	$fields = array(
					"porteros"=>"<b>PORTEROS</b>",
					"defensas"=>"<b>DEFENSAS</b>",
					"medios"=>"<b>MEDIOS</b>",
					"delanteros"=>"<b>DELANTEROS</b>",
					"total"=>"<b>TOTAL</b>"
				);
	$pdf->ezText("\n",6);			
	$pdf->ezTable($vsummary_detail, $fields, "", $options_simple);
		
	$vsummary_money[]=array("presupuesto"=>$summary->presupuesto,"gastado"=>$summary->gastado,"resto"=>$resto);
	$fields = array(
					"presupuesto"=>"<b>PRESUPUESTO</b>",
					"gastado"=>"<b>GASTADO</b>",
					"resto"=>"<b>RESTO</b>"
				);
	$pdf->ezText("\n",6);	
	$pdf->ezTable($vsummary_money, $fields, "", $options_simple);
	
	//$pdf->ezStream();
	
	return $pdf;
}

function log2cvs(){
	$oBBDD=BBDD::get_instancia();
	header("Content-Type: text/csv; charset=utf-8");
	header("Content-Disposition: attachment; filename=log-".APPNAME."-".YEAR.".csv");
	// create a file pointer connected to the output stream
	$output = fopen('php://output', 'w');

	$sql="SELECT logg.fecha fecha, logg.tipo tipo, logg.coste coste, equip.nombre nombreequipo, jug.nombre jugador FROM `".get_pref_reports()."_log` logg ";
	$sql .="INNER JOIN `".get_pref_reports()."_equipos` equip ON (logg.idprop=equip.id) INNER JOIN `".get_pref_reports()."_jugadores` jug ON (jug.id=logg.idplayer) ";
	$sql .="ORDER BY logg.id ASC";

	$obj_log=$oBBDD->get_resource($sql);
	while ($row = mysqli_fetch_assoc($obj_log)) 
		fputcsv($output, $row);

	fclose($output);
	
}

?>
