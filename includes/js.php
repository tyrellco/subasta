<?php
/*
 *      js.php
 *      
 *      
 *      Copyright 2011 Mario Rubiales Gómez <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 *      
 */

 /*
  * Esta librería general variables locales y vectores para que puedan ser leidos por Javascript
  * */
 
 	require_once(dirname(__FILE__)."/../classes/BBDD.class.php");

	//Funcion que hace lo mismo que get_pref de global.inc.php. Algo hay en este fichero que genera un warning header, para salir del paso creo otra funcion local
	function get_pref_js(){
	if (isset($_SESSION["year"]))
		return $_SESSION["year"];
	else
		return PREF;	
	}
	
	
	switch ($_GET["opt"]){
	case 1:{ /*Creamos la el array de jugadores en javascript*/
		Header("content-type: application/x-javascript");
		$oBBDD=BBDD::get_instancia();
		$sql="SELECT `id`,`nombre`, `demarcacion`,`equipolfp` FROM `".get_pref_js()."_jugadores` ORDER BY `nombre`";
		$obj_players=$oBBDD->get_resource($sql);
		$js="var players = [";
		/*
		 * Hay que poner las propiedades 'label': texto o sugerencia que aparecerá al escribir y 'value': el valor que se mostrará al seleccionar la opción*/
		while ($data=mysqli_fetch_object($obj_players)){
			$label=addslashes($data->nombre)." - ".$data->equipolfp;
			$js .="{ id: '".$data->id."', label: '".$label."', value: '".addslashes($data->nombre)."'},\n";
		}
		$js=rtrim($js,",\n");
		$js .="];\n";
		//variable global con el número de movimientos tras los cuales se hará el backup 
		$js .="var rotatebck=".ROTATEBCK.";\n";
		//variable global con el resto de dinero por debajo del cual se muestra el cuadro resumen (RESTO) en rojo
		$js .="var poor=".POOR.";\n";
		break;
	}
	case 2:{ /*Retornamos una cadena de texto que es el array de jugadores en javascript, luego lo cargaremos con jquery.*/
		$oBBDD=BBDD::get_instancia();
		$sql="SELECT `id`,`nombre`, `demarcacion`,`equipolfp` FROM `".get_pref_js()."_jugadores` ORDER BY `nombre`";
		$obj_players=$oBBDD->get_resource($sql);
		$js="var players = [";
		/*
		 * Hay que poner las propiedades 'label': texto o sugerencia que aparecerá al escribir y 'value': el valor que se mostrará al seleccionar la opción*/
		while ($data=mysqli_fetch_object($obj_players)){
			$label=$data->nombre." - ".$data->equipolfp;
			$js .="{ id: '".$data->id."', label: '".$label."', value: '".$data->nombre."'},\n";
		}
		$js=rtrim($js,",\n");
		$js .="];\n";	
		break;
	}
}
	echo $js;


?>
