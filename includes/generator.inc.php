<?php
/*
 *      generator.inc.php
 *      
 *      Copyright 2009 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");

function get_team_html_list($idteam){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `id`,`manager1` nombre FROM `".get_pref()."_equipos` ORDER BY `nombre`";
	$obj_teams=$oBBDD->get_resource($sql);
	$selected="";
	$html ="";
	$inithtml="<select id='selectteam' name='selectteam'>";

	while ($team=mysqli_fetch_object($obj_teams)){
		if ($idteam==$team->id)
			$selected="selected";
		else
			$selected="";
		$prettyname=strtolower($team->nombre);
		$html .= "<option value='$team->id' $selected>$prettyname</option>";
	}
	$endhtml="</select>";
	
	$allhtml=$inithtml.$html.$endhtml;	
	
	return $allhtml;
}


function get_lfpteams_html_list(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `".get_pref()."_equiposlfp` ORDER BY `nombre`";
	$obj_teams=$oBBDD->get_resource($sql);
	$html=<<<eof
	<select id='selectlfpteam' name='selectlfpteam'>"
  <option value='XXX' selected>selecciona equipo....</option>
eof;
	while ($team=mysqli_fetch_object($obj_teams)){
		$nombre=$team->nombre;
		$html .= "<option value='$team->codigo'>$nombre</option>";
	}	
	$html .="</select>";
	
	return $html;
}

function get_record_player($idplayer,$modify){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT jug.id id, jug.nombre nombre, jug.idprop idprop, jug.demarcacion dem,jug.coste coste,jug.equipolfp cod, lfp.nombre equipo, equip.nombre nomprop ";
	$sql .="FROM `".get_pref()."_jugadores` jug INNER JOIN `".get_pref()."_equiposlfp` lfp ON (lfp.codigo=jug.equipolfp)  INNER JOIN `".get_pref()."_equipos` equip ON (jug.idprop=equip.id) WHERE jug.id=".$idplayer;
	$obj_player=$oBBDD->get_resource($sql);
	$player=mysqli_fetch_object($obj_player);
	$playername=$player->nombre;
	$lfpteam=$player->equipo;
	$nomprop=$player->nomprop;
	$list_team=get_team_html_list($player->idprop);
	$cost=$player->coste==0?"":$player->coste;
	
	$sql="SELECT count(*) hay FROM `".get_pref()."_formacion` WHERE `idjugador`=".$idplayer;
	$obj_extra=$oBBDD->get_resource($sql);
	$extra=mysqli_fetch_object($obj_extra);
	$asterisk=$extra->hay==0?"":"<span class='formacion' title='jugador con derechos de formación'>*</span>";
	
	switch ($player->dem) {
		case '1': $demtxt="Portero";
		break;
		case '2': $demtxt="Defensa";
		break;
		case '3': $demtxt="Centrocampista";
		break;
		case '4': $demtxt="Delantero";
		break;
		default: $demtxt="Pendiente";
		break;
	}
	
	if ( ($player->idprop==0) || ($modify) ){
			$html=<<<eof
			<input type="hidden" id="idplayer" name"idplayer" value="{$player->id}"/>
			<input type="hidden" id="iddem" name"iddem" value="{$player->dem}"/>
			<input type="hidden" id="oldprop" name"olprop" value="{$player->idprop}"/>
			<input type="hidden" id="oldcost" name"oldcost" value="{$player->coste}"/>
			<div id="escudo"><img src="images/escudos/{$player->cod}.png"/></div>
			<ul id='recordplayer'>
			<li class='playername'>{$playername}{$asterisk}</li>
			<li class='playerdem'>{$demtxt}</li>
			<li class='playerlfp'>{$lfpteam}</li>
			<li><br/></li>
			<li class='playerowner'><span>Propietario: {$list_team}</li>
			<li class='playercost'><span>Coste: </span><input type="text" id="cost" name="cost" value="{$cost}"/></li>
			</ul>
			<div id="btns_record">
				<table>
					<tr>
						<td><input type="button" id="btnsave" name="btnsave" class="accept" value="Guardar"/></td>
						<td><input type="button" id="btncancel" name="btncancel" class="cancel" value="Cancelar"/></td>
					</tr>
				</table>
			</div>
eof;
	}
	else{
		$html=<<<eof
			<div id="escudo"><img src="images/fichado.png"/></div>
			<ul id='recordplayer'>
			<li class='playername'>{$playername}</li>
			<li class='playerdem'>{$demtxt}</li>
			<li class='playerlfp'>{$lfpteam}</li>
			<li><br/></li>
			<li class='playerowner'><span class="newprop">Propietario: {$nomprop}</span></li>
			<li class='playercost'><span class="newprop">Coste: {$cost} kilazos</span></li>
			</ul>
			<div id="btns_record">
				<table>
					<tr>
						<td><input type="button" id="btnmod" name="btnmod" class="modify" value="Modificar"/></td>
					</tr>
				</table>
			</div>
eof;
	}
	return $html;
}


function get_record_player_readonly($idplayer){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT jug.id id, jug.nombre nombre, jug.idprop idprop, jug.demarcacion dem,jug.coste coste,jug.equipolfp cod, lfp.nombre equipo, equip.nombre nomprop ";
	$sql .="FROM `".get_pref()."_jugadores` jug INNER JOIN `".get_pref()."_equiposlfp` lfp ON (lfp.codigo=jug.equipolfp)  INNER JOIN `".get_pref()."_equipos` equip ON (jug.idprop=equip.id) WHERE jug.id=".$idplayer;
	$obj_player=$oBBDD->get_resource($sql);
	$player=mysqli_fetch_object($obj_player);
	$playername=$player->nombre;
	$lfpteam=$player->equipo;
	$nomprop=$player->nomprop;
	$cost=$player->coste==0?"":$player->coste;
	
	//miramos a ver si este jugador tiene derechos de formación
	$sql="SELECT count(*) hay FROM `".get_pref()."_formacion` WHERE `idjugador`=".$idplayer;
	$obj_extra=$oBBDD->get_resource($sql);
	$extra=mysqli_fetch_object($obj_extra);
	$asterisk=$extra->hay==0?"":"<span class='formacion' title='jugador con derechos de formación'>*</span>";
	
	switch ($player->dem) {
		case '1': $demtxt="Portero";
		break;
		case '2': $demtxt="Defensa";
		break;
		case '3': $demtxt="Centrocampista";
		break;
		case '4': $demtxt="Delantero";
		break;
		default: $demtxt="Pendiente";
		break;
	}
	
	if ($player->idprop==0)
		$html="<div id='escudo'><img src='images/escudos/$player->cod.png'/></div>";
	else
		$html="<div id='escudo'><img src='images/fichado.png'/></div>";
		
	$html .=<<<eof
		<ul id='recordplayer'>
		<li class='playername'>{$playername}{$asterisk}</li>
		<li class='playerdem'>{$demtxt}</li>
		<li class='playerlfp'>{$lfpteam}</li>
		<li><br/></li>
		<li class='playerowner'><span>Propietario: </span>{$nomprop}</li>
		<li class='playercost'><span>Coste: </span>{$cost}</li>
		</ul>
		<div id="btns_record">
		<table>
			<tr>
				<td><input type="button" id="btncancel" name="btncancel" class="cancel" value="Cerrar"/></td>
			</tr>
		</table>
	</div>
eof;

	return $html;
}


/*
 * return 0: todo correcto, se puede hacer el fichaje
 * return 1: Error, no tiene dinero suficiente para fichar el jugador
 * return 2: Error, ya tiene la plantilla completa, no puede fichar más jugadores
 * return 3: Error, no tendríamos dinero para completar la plantilla
 * */
function check_transfer($idplayer,$idnewprop,$idoldprop,$cost){

	$oBBDD=BBDD::get_instancia();
	$sql="SELECT *,(porteros+defensas+medios+delanteros) AS total FROM `".get_pref()."_resumen` WHERE `idequipo`=$idnewprop";
	$obj_summary=$oBBDD->get_resource($sql);
	$summary=mysqli_fetch_object($obj_summary);
	$rest=$summary->presupuesto - $summary->gastado;
	$total=$summary->total+1;// Al total de jugadores le sumo uno porque para hacer los cálculos necesito suponer que el fichaje se hace
	
	//solo analizamos esto si el propietario nuevo y antiguo son diferentes, ya que si fueran el mismo no se estaría aumentando el total de jugadores
	if ($idnewprop!=$idoldprop){
		if ($summary->total >= MAXNUMBER_PLAYERS) 
			return 2; // Ya tiene la plantilla completa, no puede fichar más jugadores
	}
	else{ //el propietario es el mismo por lo tanto cambiará sólo el coste, analizamos el coste real mirando el coste anterior del jugador
		$sql="SELECT `coste` FROM `".get_pref()."_jugadores` WHERE `id`=$idplayer";
		$obj_player=$oBBDD->get_resource($sql);
		$player=mysqli_fetch_object($obj_player);
		$cost=$cost - $player->coste; //este es el coste real del jugador, la diferencia del coste anterior y el actual
		$total=$summary->total; //El total de jugadores NO le sumo uno ya que al ser el mismo propietario no se aumenta el número de jugadores
	}
			
		
	if ($cost>$rest)
		return 1; //No tiene dinero suficiente para fichar a este jugador
		
	$available_money=$rest-$cost; //dinero que tendría disponible si se hace el fichaje
	
	/* si aún no ha fichado el minimo exigible analizamos si con el dinero que le queda tendrá suficiente para hacerlo, al total de jugadores le sumo uno
	 * porque para hacer los cálculos necesito suponer que el fichaje se hace*/
	if (($total) < MINNUMBER_PLAYERS){ //si aún no se ha fichado lo mínimo exigible
		$available_count=MINNUMBER_PLAYERS - $total; //plazas disponibles hasta llegar a las mínimas exigidas (MINNUMBER_PLAYERS)
		$required_money=$available_count * MINPRICE; //dinero necesario (como mínimo) para completar una plantilla de MINNUMBER_PLAYERS (15)
		if ($required_money > $available_money){
			echo $required_money."#".$available_money;
			return 3; // no tendríamos dinero para completar la plantilla
		} 
			
	}
		
	return 0;	//el fichaje se puede hacer	
	
}

function post_transfer ($idprop){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT resum.presupuesto presupuesto, resum.gastado gastado, (resum.porteros+resum.defensas+resum.medios+resum.delanteros) AS total,";
	$sql .="eq.nombre equipo FROM `".get_pref()."_resumen` resum INNER JOIN `".get_pref()."_equipos` eq ON (eq.id=resum.idequipo) WHERE resum.idequipo=".$idprop;
	$obj_summary=$oBBDD->get_resource($sql);
	$summary=mysqli_fetch_object($obj_summary);
	$rest=$summary->presupuesto - $summary->gastado;
	
	$info="";
	if ($summary->total==MAXNUMBER_PLAYERS)
		$info="Fin de la subasta para ".$summary->equipo.", ".MAXNUMBER_PLAYERS." jugadores fichados. Gracias por participar";
	else{
		if ($rest<MINPRICE)
			$info="Fin de la subasta para ".$summary->equipo.", tiene ".$rest." kilos en caja. Gracias por participar";
	}
	
	return $info;
}

function set_extra_formac($idplayer,$cost){
	$oBBDD=BBDD::get_instancia();
	$extra=0;
	$xml_formation="";
	$sql="SELECT count(*) hay,idprop,extra FROM `".get_pref()."_formacion` WHERE `idjugador`=".$idplayer;
	$obj_formac=$oBBDD->get_resource($sql);
	$formacion=mysqli_fetch_object($obj_formac);
	if ($formacion->hay > 0){
		//si coste>0 es un fichaje, se actualiza la pasta que se lleva alguien por lo derechos de formación
		if ($cost>0){
			$extra= $cost * PERCENTFORMAC/100;
			if ($extra>MINPRICE){ //si el porcentaje no es mayor de 20 kilos, se desecha y sólo se ingresa la cantidad fija
				$extra +=EXTRAFORMAC;
				if ($extra > MAXEXTRA) //comprobamos que no se obtenga más de MAXEXTRA por derechos de formación
					$extra=MAXEXTRA;
			}	
			else
				$extra=EXTRAFORMAC;
			$sql="UPDATE `".get_pref()."_formacion` SET `extra`=".$extra." WHERE `idjugador`=".$idplayer;
			$oBBDD->set_resultados($sql);
			$sql="UPDATE `".get_pref()."_resumen` SET `presupuesto`=`presupuesto`+".$extra." WHERE `idequipo`=".$formacion->idprop;
			$oBBDD->set_resultados($sql);
			$xml_formation=get_xml_formation($idplayer);
		}
		else{ //coste=0, es un reset, se devuelve la pasta de los derechos de formación al modificarse el fichaje
			$sql="UPDATE `".get_pref()."_formacion` SET `extra`=0 WHERE `idjugador`=".$idplayer;
			$oBBDD->set_resultados($sql);
			$sql="UPDATE `".get_pref()."_resumen` SET `presupuesto`=`presupuesto`-".$formacion->extra." WHERE `idequipo`=".$formacion->idprop;
			$oBBDD->set_resultados($sql);
		}		
	}
	return $xml_formation;
}

/*Hacemos el fichaje*/
function set_transfer ($idplayer,$idoldprop,$idnewprop,$cost,$oldcost,$dem){
	$checkresult=$idnewprop!=0?check_transfer($idplayer,$idnewprop,$idoldprop,$cost):0;
	$xml_formation="";
	$info="";
	if ($checkresult==0){ //no hay problemas para hacer el fichaje, adelante
		$oBBDD=BBDD::get_instancia();
		$vlog=array();
		//Por defecto el movimiento es un fichaje
		$vlog["tipo"]="FICH";
		
		switch ($dem) {
			case '1': $sqldem="`porteros`=`porteros`";
			break;
			case '2': $sqldem="`defensas`=`defensas`";
			break;
			case '3': $sqldem="`medios`=`medios`";
			break;
			case '4': $sqldem="`delanteros`=`delanteros`";
			break;
		}
		
		if ($idoldprop!=0){ //El jugador ya tenía un propietario (modificación) le devolvemos la pasta y le descontamos la demarcación
			$sql="UPDATE `".get_pref()."_resumen` SET `gastado`=`gastado`-$oldcost, ".$sqldem."-1 WHERE `idequipo`=$idoldprop";
			$oBBDD->set_resultados($sql);
			//hacemos un reset de la pasta ganado por derechos de formación
			$xml_formation=set_extra_formac($idplayer,0);
			//Es una modificación
			$vlog["tipo"]="MODIF";
		}
	
		if ($idnewprop==0){ //Si el nuevo propietario es 'jugador libre' el coste lo pongo a cero.
			$cost=0;
			//Un reset, el jugador vuelve a ser libre
			$vlog["tipo"]="RESET";
		}
		// Asignamos el jugador a su nuevo propietario	
		$sql="UPDATE `".get_pref()."_jugadores` SET `idprop`=$idnewprop, `coste`=$cost WHERE `id`=$idplayer";
		$oBBDD->set_resultados($sql);
		
		$vlog["idprop"]=$idnewprop;
		$vlog["coste"]=$cost;
		$vlog["idplayer"]=$idplayer;
		$vlog["fecha"]=date("H:i:s");
		
		// Actualizamos el dinero gastado y el contador de la demarcación del nuevo propietario siempre que no sea un jugador libre
		if ($idnewprop!=0){
			$sql="UPDATE `".get_pref()."_resumen` SET `gastado`=`gastado`+$cost, ".$sqldem."+1 WHERE `idequipo`=$idnewprop";
			$oBBDD->set_resultados($sql);
			$xml_formation=set_extra_formac($idplayer,$cost);
		}
		
		//Actualizamos el log
		set_log($vlog);
		
		//Obtenemos el resumen para los equipos implicados
		$xml_summary=get_xml_summary($idnewprop,$idoldprop);
		//Obtenemos el log de este último movimiento 
		$xml_log=get_xml_log($vlog);
		
		//Comprobamos si hay información extra que ofrecer (fin de subasta ...)
		if ($idnewprop!=0) //sólo para fichajes reales, no para establecer un jugador libre (idnewprop=0)
			$info=post_transfer($idnewprop);
			
		$xml=<<<eof
		<?xml version="1.0" encoding="utf-8"?>
		<data>
			<error id="0">todo correcto</error>
			<info>{$info}</info>
			{$xml_summary}
			{$xml_log}
			{$xml_formation}
		</data>
eof;
}// no tiene plazas o dinero suficiente para hacer este fichaje
else{
		switch ($checkresult) {
			case '1': $txterror="No tienes dinero suficiente para fichar a este jugador";
			break;
			case '2': $txterror="Ya tienes la plantilla completa, no puedes fichar más jugadores";
			break;
			case '3': $txterror="Imposible, si ficharas a este jugador no te quedaría pasta para completar la plantilla";
			break;
		}
			$xml=<<<eof
		<?xml version="1.0" encoding="utf-8"?>
		<data>
			<error id="{$checkresult}">{$txterror}</error>
		</data>
eof;
}	
		return $xml;
}

function set_log($vlog){
	$oBBDD=BBDD::get_instancia();
	$sql="INSERT INTO `".get_pref()."_log` (`fecha`,`tipo`,`idprop`, `idplayer`,`coste`,`subastador`) ";
	$sql .="VALUES ('".$vlog["fecha"]."','".$vlog["tipo"]."',".$vlog["idprop"].",".$vlog["idplayer"].",".$vlog["coste"].",'".$_SESSION['subastador']."')";
	$oBBDD->set_resultados($sql); 
}

function get_xml_log($vlog){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT logg.id idlog, equip.nombre nombreequipo, jug.nombre jugador FROM `".get_pref()."_log` logg INNER JOIN `".get_pref()."_equipos` ";
	$sql .="equip ON (logg.idprop=equip.id) INNER JOIN `".get_pref()."_jugadores` jug ON (jug.id=logg.idplayer) ORDER BY idlog DESC LIMIT 1";
	$obj_log=$oBBDD->get_resource($sql);
	$log=mysqli_fetch_object($obj_log);
		
	$fecha=$vlog["fecha"];
	$tipo=$vlog["tipo"];
	$coste=$vlog["coste"];
	$jugador=$log->jugador;
	$equipo=$log->nombreequipo;
	
	$xml=<<<eof
		<log>
			<fecha>{$fecha}</fecha>
			<tipo>{$tipo}</tipo>
			<coste>{$coste}</coste>
			<team>{$equipo}</team>
			<player>{$jugador}</player>
		</log>
eof;

	return $xml;
}

function get_xml_formation($idplayer){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT f.extra extra, e.nombre team,e.id idteam, j.nombre playername FROM `".get_pref()."_formacion` f INNER JOIN `".get_pref()."_equipos` e ";
	$sql .="ON (f.idprop=e.id) INNER JOIN `".get_pref()."_jugadores` j ON (f.idjugador=j.id) WHERE f.idjugador=".$idplayer;
	$obj_formac=$oBBDD->get_resource($sql);
	$formacion=mysqli_fetch_object($obj_formac);
	$playername=$formacion->playername;
	
	$xml=<<<eof
		<formation>
			<extra>{$formacion->extra}</extra>
			<team>{$formacion->team}</team>
			<idteam>{$formacion->idteam}</idteam>
			<playername>{$playername}</playername>
		</formation>
eof;

	return $xml;
}

function get_xml_summary($newprop,$oldprop){
	$oBBDD=BBDD::get_instancia();
	
	if ( ($newprop!=0) && ($oldprop!=0) ) //es un cambio, se muestra el resumen ambos equipos
		$where="idequipo=$newprop OR idequipo=$oldprop";
	else{
		if ( ($newprop!=0) && ($oldprop==0) ) //un fichaje, muestro sólo el resumen del comprador
			$where="idequipo=$newprop";
		else{
			if ( ($newprop==0) && ($oldprop!=0) )//un reset (poner un jugador como libre)
				$where="idequipo=$oldprop";
			//else -> los dos a cero no se plantea ya que es imposible llegar aquí de esa manera
		}
	}		
	
	$sql="SELECT resum.idequipo idequipo, resum.presupuesto presupuesto, resum.gastado gastado, resum.porteros por, ";
	$sql .="resum.defensas def, resum.medios med, resum.delanteros del, equip.nombre nombre FROM `".get_pref()."_resumen` ";
	$sql .="resum INNER JOIN `".get_pref()."_equipos` equip ON (idequipo=equip.id) WHERE $where";
	$obj_summary=$oBBDD->get_resource($sql);
	$xml="";
	while ($summary=mysqli_fetch_object($obj_summary)){
		$resto=$summary->presupuesto - $summary->gastado;
		$total=$summary->por + $summary->def + $summary->med + $summary->del;
		$xml .=<<<eof
		<summary id="{$summary->idequipo}">
			<presupuesto>{$summary->presupuesto}</presupuesto>
			<gastado>{$summary->gastado}</gastado>
			<resto>{$resto}</resto>
			<por>{$summary->por}</por>
			<def>{$summary->def}</def>
			<med>{$summary->med}</med>
			<del>{$summary->del}</del>
			<total>{$total}</total>
		</summary>
eof;
	}
	
	return $xml;
}

function get_just_xml_summary($idteam){
	$oBBDD=BBDD::get_instancia();
	
	$sql="SELECT resum.idequipo idequipo, resum.presupuesto presupuesto, resum.gastado gastado, resum.porteros por, ";
	$sql .="resum.defensas def, resum.medios med, resum.delanteros del, equip.nombre nombre FROM `".get_pref()."_resumen` ";
	$sql .="resum INNER JOIN `".get_pref()."_equipos` equip ON (idequipo=equip.id) WHERE `idequipo`=".$idteam;
	$obj_summary=$oBBDD->get_resource($sql);
	$summary=mysqli_fetch_object($obj_summary);
	$xml="";
	$resto=$summary->presupuesto - $summary->gastado;
	$total=$summary->por + $summary->def + $summary->med + $summary->del;
	$xml=<<<eof
	<?xml version="1.0" encoding="utf-8"?>
	<data>
		<summary id="{$summary->idequipo}">
			<presupuesto>{$summary->presupuesto}</presupuesto>
			<gastado>{$summary->gastado}</gastado>
			<resto>{$resto}</resto>
			<por>{$summary->por}</por>
			<def>{$summary->def}</def>
			<med>{$summary->med}</med>
			<del>{$summary->del}</del>
			<total>{$total}</total>
		</summary>
	</data>
eof;

return $xml;
}

function get_summary($id){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT resum.idequipo idequipo, resum.presupuesto presupuesto, resum.gastado gastado, resum.porteros por, ";
	$sql .="resum.defensas def, resum.medios med, resum.delanteros del, equip.nombre nombre FROM `".get_pref()."_resumen` ";
	$sql .="resum INNER JOIN `".get_pref()."_equipos` equip ON (idequipo=equip.id) WHERE idequipo=$id";
	$obj_summary=$oBBDD->get_resource($sql);
	$html="";
	$summary=mysqli_fetch_object($obj_summary);
	$resto=$summary->presupuesto - $summary->gastado;
	$restoclass=$resto<POOR?"srestow":"sresto";
	$total=$summary->por + $summary->def + $summary->med + $summary->del;
	$teamname=strtoupper($summary->nombre);
	$html .=<<<eof
	<div id="dsummary_report">
	<table class="tsummary" id="id_{$summary->idequipo}">
				<tr><th colspan="6" class="headteam">{$teamname}</th></tr>
				<tr>
					<th colspan="2">PRESUPUESTO</th>
					<th colspan="2">GASTADO</th>
					<th colspan="2">RESTO</th>
				</tr>
				<tr>
					<td colspan="2" class="spresupuesto">{$summary->presupuesto}</td>
					<td colspan="2" class="sgastado">{$summary->gastado}</td>
					<td colspan="2" class="{$restoclass}">{$resto}</td>
				</tr>
				<tr>
					<th>POR</th>
					<th>DEF</th>
					<th>MED</th>
					<th>DEL</th>
					<th colspan="2">TOTAL</th>
				</tr>
				<tr>
					<td class="spor">{$summary->por}</td>
					<td class="sdef">{$summary->def}</td>
					<td class="smed">{$summary->med}</td>
					<td class="sdel">{$summary->del}</td>
					<td colspan="2" class="stotal">{$total}</td>
				</tr>
			</table>
			</div>
eof;
	
	echo $html;
}

function get_full_summary(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT resum.idequipo idequipo, resum.presupuesto presupuesto, resum.gastado gastado, resum.porteros por, ";
	$sql .="resum.defensas def, resum.medios med, resum.delanteros del, equip.nombre nombre FROM `".get_pref()."_resumen` ";
	$sql .="resum INNER JOIN `".get_pref()."_equipos` equip ON (idequipo=equip.id) ORDER BY equip.nombre";
	$obj_summary=$oBBDD->get_resource($sql);
	$html="";
	while ($summary=mysqli_fetch_object($obj_summary)){
		$resto=$summary->presupuesto - $summary->gastado;
		$restoclass=$resto<POOR?"srestow":"sresto";
		$total=$summary->por + $summary->def + $summary->med + $summary->del;
		$teamname=strtoupper($summary->nombre);
		$html .=<<<eof
		<table class="tsummary" id="ideq_{$summary->idequipo}">
					<tr><th colspan="6" title="obtener informe para este equipo" id="id_{$summary->idequipo}" class="headteam">{$teamname}</th></tr>
					<tr>
						<th colspan="2">PRESUPUESTO</th>
						<th colspan="2">GASTADO</th>
						<th colspan="2">RESTO</th>
					</tr>
					<tr>
						<td colspan="2" class="spresupuesto">{$summary->presupuesto}</td>
						<td colspan="2" class="sgastado">{$summary->gastado}</td>
						<td colspan="2" class="{$restoclass}">{$resto}</td>
					</tr>
					<tr>
						<th>POR</th>
						<th>DEF</th>
						<th>MED</th>
						<th>DEL</th>
						<th colspan="2">TOTAL</th>
					</tr>
					<tr>
						<td class="spor">{$summary->por}</td>
						<td class="sdef">{$summary->def}</td>
						<td class="smed">{$summary->med}</td>
						<td class="sdel">{$summary->del}</td>
						<td colspan="2" class="stotal">{$total}</td>
					</tr>
				</table>
eof;
	}
	
	echo $html;
}

function get_full_log($orderby){
	
	$idhora="order_1";
	$idequipo="order_2";
	$idjugador="order_3";
	$idcoste="order_4";
	
	switch ($orderby) {
			case "0":
			case "order_0":
			case "order_1":		$order="logg.id DESC";
							 					$idhora="order_11";	
			break;
			case "order_11":	$order="logg.id ASC";
												$idhora="order_1";	
			break;
			case "order_2": 	$order="nombreequipo ASC";
												$idequipo="order_22";
			break;
			case "order_22":	$order="nombreequipo DESC";
								 				$idequipo="order_2";
			break;
			case "order_3": 	$order="jugador ASC";
												$idjugador="order_33";
			break;
			case "order_33":	$order="jugador DESC";
												$idjugador="order_3";
			break;
			case "order_4": 	$order="logg.coste DESC";
												$idcoste="order_44";
			break;
			case "order_44": 	$order="logg.coste ASC";
								 				$idcoste="order_4"; 	
			break;
	}
	
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT logg.fecha, logg.tipo, logg.coste, equip.nombre nombreequipo, jug.nombre jugador FROM `".get_pref()."_log` logg ";
	$sql .="INNER JOIN `".get_pref()."_equipos` equip ON (logg.idprop=equip.id) INNER JOIN `".get_pref()."_jugadores` jug ON (jug.id=logg.idplayer) ";
	$sql .="ORDER BY $order";

	$obj_log=$oBBDD->get_resource($sql);

	$html="<table class='tlog'>";
	$html .="<tr id='headlog'><th id='$idhora' class='sortable'>HORA</th><th id='$idequipo' class='sortable'>EQUIPO</th>";
	$html.="<th id='$idjugador' class='sortable'>JUGADOR</th><th id='$idcoste' class='sortable'>COSTE</th></tr>";
	while ($log=mysqli_fetch_object($obj_log)){
		$equipo=$log->nombreequipo;
		$jugador=$log->jugador;
		$html .="<tr><td title='$log->tipo'>$log->fecha</td><td>$equipo</td><td>$jugador</td><td>$log->coste</td></tr>";
	}
	$html .="</table>";
	
	echo $html;
}

function get_count_transfers(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT COUNT(*) cont FROM `".get_pref()."_log`";
	$obj_log=$oBBDD->get_resource($sql);
	$log=mysqli_fetch_object($obj_log);
	echo $log->cont;
}

function get_lfpteams(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `".get_pref()."_equiposlfp` ORDER BY `nombre`";
	$obj_teams=$oBBDD->get_resource($sql);
	$html="<table class='tlfpteams'>";
	while ($team=mysqli_fetch_object($obj_teams)){
		$equipo=$team->nombre;
		$html .="<tr><td id='$team->codigo'>$equipo</td></tr>";
	}
	$html .="</table>";
	
	return $html;
}

function get_lfpteams_desktop(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `".get_pref()."_equiposlfp` ORDER BY `nombre`";
	$obj_teams=$oBBDD->get_resource($sql);
	$html="<table class='tlfpteams'>";
	while ($team=mysqli_fetch_object($obj_teams)){
		$equipo=$team->nombre;
		$html .="<tr><td id='$team->codigo' class='desktop'>$equipo</td></tr>";
	}
	$html .="</table>";
	
	return $html;
}

function get_players4teams($cod){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `id`,`idprop`,`demarcacion`,`nombre` FROM `".get_pref()."_jugadores` WHERE `equipolfp`='$cod' ORDER BY `demarcacion`,`nombre`";
	$obj_players=$oBBDD->get_resource($sql);
	$html="<table class='tplayers4teams'>";
	while ($player=mysqli_fetch_object($obj_players)){
		switch ($player->demarcacion) {
			case '1': $demtxt="POR";
								$class="datapor";
			break;
			case '2': $demtxt="DEF";
								$class="datadef";
			break;
			case '3': $demtxt="MED";
								$class="datamed";
			break;
			case '4': $demtxt="DEL";
								$class="datadel";
			break;
			default: $demtxt="PTE";
							 $class="";	
			break;
		}
		$statustxt="F";
		$statusclass="nonfree";
		$statustitle="jugador ya fichado";
		if ($player->idprop==0){
			$statustxt="L";
			$statusclass="free";
			$statustitle="jugador libre";
		}
		$nombre=$player->nombre;
		$html .="<tr class='$class'><td class='tddem'>$demtxt</td><td id='$player->id' class='tdplayername'>$nombre</td><td class='$statusclass' title='$statustitle'>$statustxt</td></tr>";
	}
	$html .="</table>";
	
	return $html;
}

function get_form_newplayer(){
	$dem_list=<<<eof
	<select id='selectdem' name='selectdem'>
		<option value="0" selected>demarcación ...</option>
		<option value="1">portero</option>
		<option value="2">defensa</option>
		<option value="3">medio</option>
		<option value="4">delantero</option>
	</select>
eof;

	$lfpteams_list=get_lfpteams_html_list();
	$html=<<<eof
	<div id="escudo"><img src="images/nuevo.png"/></div>
	<div id="dnewplayer">
	<form id="form_new_player" name="form_new_player" action="#">
	<table>
		<tr><th>Nombre:</th><td><input type="text" id="nameplayer" name="nameplayer" value=""/></td></tr>
		<tr><th>Demarcaci&oacute;n:</th><td>{$dem_list}</td></tr>
		<tr><th>Equipo:</th><td>{$lfpteams_list}</td></tr>
	</table>
	</form>
	</div>
	<div id="btns_record">
		<table>
			<tr>
				<td><input type="button" id="btnsave" name="btnsave" class="accept" value="Guardar"/></td>
				<td><input type="button" id="btncancel" name="btncancel" class="cancel" value="Cancelar"/></td>
			</tr>
		</table>
	</div>
eof;

return $html;
}

function set_new_player($vplayer){
	$oBBDD=BBDD::get_instancia();
	//generamos un nuevo id hasta que sepamos definitivamente el que le da la LFM
	$myid=date("His");
	$sql="INSERT INTO `".get_pref()."_jugadores` (`id`,`nombre`,`demarcacion`, `equipolfp`) ";
	$sql .="VALUES ($myid,'".utf8_decode($vplayer["nameplayer"])."',".$vplayer['selectdem'].",'".$vplayer["selectlfpteam"]."')";
	$oBBDD->set_resultados($sql);
	
	$vlog=array();
	$vlog["tipo"]="NEW";
	$vlog["idprop"]=0;
	$vlog["coste"]=0;
	$vlog["idplayer"]=$myid;
	$vlog["fecha"]=date("H:i:s");
	
	//Actualizamos el log
	set_log($vlog);
	
	//Obtenemos el log de este último movimiento 
	$xml_log=get_xml_log($vlog);
	
	$xml=<<<eof
	<?xml version="1.0" encoding="utf-8"?>
	<data>
		<error id="0">todo correcto</error>
		<newplayer>{$myid}</newplayer>
		{$xml_log}
	</data>
eof;

	return $xml;
}

function get_report($id){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=$id ORDER BY `demarcacion`,`nombre`";

	$obj_players=$oBBDD->get_resource($sql);
	$html="<div id='close_report'><a href='includes/reports.inc.php?opt=1&idequipo=$id' target='_blank'>";
	$html .="<img src='images/export.png' alt='exportar a pdf' title='exportar a pdf' name='export' id='export'/></a>";
	$html .="<img src='images/close.png' alt='cerrar' title='cerrar' id='close' name='close'/></div>";
	$html .="<div id='dplayers_report'>";
	$html .="<table class='treport'>";
	$html .="<tr><th>Nº</th><th>JUGADOR</th><th>DEMARCACI&Oacute;N</th><th>LFP</th><th>COSTE</th></tr>";
	$count=1;
	while ($player=mysqli_fetch_object($obj_players)){
		$nombre=$player->nombre;
		switch ($player->demarcacion) {
			case '1': $demtxt="Portero";
								$class="datapor";
			break;
			case '2': $demtxt="Defensa";
								$class="datadef";
			break;
			case '3': $demtxt="Medio";
								$class="datamed";
			break;
			case '4': $demtxt="Delantero";
								$class="datadel";
			break;
			default: $demtxt="Pendiente";
							 $class="";	
			break;
		}
		$html .="<tr class='$class'><td class='countreport'>$count</td><td class='nameplayerreport'>$nombre</td>";
		$html .="<td>$demtxt</td><td>$player->equipolfp</td><td>$player->coste</td></tr>";
		$count ++;
	}
	$html .="</table>\n</div>";	
		
	return $html;
}

function get_xml_graph(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT res.presupuesto presupuesto, res.gastado gastado,eq.manager1 manager FROM `".get_pref()."_resumen` res INNER JOIN `".get_pref()."_equipos` eq ON (res.idequipo=eq.id) ORDER BY eq.manager1";
	$obj_graph=$oBBDD->get_resource($sql);
	$xmldata="";
	while ($graph=mysqli_fetch_object($obj_graph)){
		$resto=$graph->presupuesto - $graph->gastado;
		$xmldata .="<gastado id='".ucfirst($graph->manager)."'>".$resto."</gastado>";
	}
	
	$xml=<<<eof
	<?xml version="1.0" encoding="utf-8"?>
	<graph>
		{$xmldata}
	</graph>
eof;

return $xml;
}
?>

