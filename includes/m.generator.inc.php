<?php
/*
 *      m.generator.inc.php
 *      
 *      Copyright 2014 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");



function get_full_summary_collapsible(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT resum.idequipo idequipo, resum.presupuesto presupuesto, resum.gastado gastado, resum.porteros por, ";
	$sql .="resum.defensas def, resum.medios med, resum.delanteros del, equip.nombre nombre FROM `".get_pref()."_resumen` ";
	$sql .="resum INNER JOIN `".get_pref()."_equipos` equip ON (idequipo=equip.id) ORDER BY equip.nombre";
	$obj_summary=$oBBDD->get_resource($sql);
	$html="";
	while ($summary=mysqli_fetch_object($obj_summary)){
		$resto=$summary->presupuesto - $summary->gastado;
		$restoclass=$resto<POOR?"srestow":"sresto";
		$total=$summary->por + $summary->def + $summary->med + $summary->del;
		$teamname=strtolower($summary->nombre);
		$html .=<<<eof
		<div id="idteam_{$summary->idequipo}" class="summary_team">
			<h1>{$teamname}</h1>
			<ul data-role="listview">
				<li data-role="list-divider">resumen fichajes</li>
				<li>porteros: {$summary->por}</li>
				<li>defensas: {$summary->def}</li>
				<li>centrocampistas: {$summary->med}</li>
				<li>delanteros: {$summary->del}</li>
				<li>total fichados: {$total}</li>
				<li data-role="list-divider">resumen dinero</li>
				<li>presupuesto: {$summary->presupuesto}</li>
				<li>gastado: {$summary->gastado}</li>
				<li>resto: {$resto}</li>
			</ul>
			<div class="pretty">
				<a href="#" id="$summary->idequipo" class="ui-btn ui-btn-inline ui-btn-icon-left ui-icon-user ui-corner-all ui-shadow report_team">
					ver fichajes
				</a>
		</div>
		</div>
eof;
	}
	
	echo $html;
}

function get_global_summary(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT resum.idequipo idequipo, resum.presupuesto presupuesto, resum.gastado gastado, resum.porteros por, ";
	$sql .="resum.defensas def, resum.medios med, resum.delanteros del, equip.nombre nombre, (resum.presupuesto)-(resum.gastado) resto FROM `".get_pref()."_resumen` ";
	$sql .="resum INNER JOIN `".get_pref()."_equipos` equip ON (idequipo=equip.id) ORDER BY resto DESC, equip.nombre";
	$obj_summary=$oBBDD->get_resource($sql);
	$html=<<<eof
	<table id="table_golbalsummary" data-role="table" data-mode="columntoggle" class="ui-responsive" id="table_global_summary" data-column-btn-text="columnas">
	<thead>
		<tr>
			<th>equipo</th>
			<th data-priority="3">fichajes</th>
			<th data-priority="5">presupuesto</th>
			<th data-priority="4">gastado</th>
			<th data-priority="1">resto</th>
		</tr>
	</thead>
	<tbody>
eof;

	while ($summary=mysqli_fetch_object($obj_summary)){
		//$resto=$summary->presupuesto - $summary->gastado;
		$restoclass=$summary->resto<POOR?"srestow":"sresto";
		$teamname=strtolower($summary->nombre);
		$total=$summary->por + $summary->def + $summary->med + $summary->del;
		$html .=<<<eof
			<tr>
				<td>{$teamname}</td>
				<td>{$total}</td>
				<td>{$summary->presupuesto}</td>
				<td>{$summary->gastado}</td>
				<td>{$summary->resto}</td>
			</tr>
eof;
	}
	$html .=<<<eof
		</tbody>
  </table>
eof;

	echo $html;
}

function get_players4teams_mobile($cod){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT `id`,`idprop`,`demarcacion`,`nombre` FROM `".get_pref()."_jugadores` WHERE `equipolfp`='$cod' ORDER BY `demarcacion`,`nombre`";
	$obj_players=$oBBDD->get_resource($sql);
	$current_dem=1;
	$current_demtxt="PORTEROS";
	$html="<ul data-role='listview'>";
	$html .="<li data-role='list-divider'>$current_demtxt</li>";
	while ($player=mysqli_fetch_object($obj_players)){
		$statustxt="fichado";
		$statusclass="nonfree";
		$statustitle="jugador ya fichado";
		if ($player->idprop==0){
			$statustxt="libre";
			$statusclass="free";
			$statustitle="jugador libre";
		}
		$nombre=$player->nombre;
		if ($current_dem!=$player->demarcacion){
			switch ($player->demarcacion) {
				case '1': $demtxt="PORTEROS";
									$class="datapor";
				break;
				case '2': $demtxt="DEFENSAS";
									$class="datadef";
				break;
				case '3': $demtxt="MEDIOS";
									$class="datamed";
				break;
				case '4': $demtxt="DELANTEROS";
									$class="datadel";
				break;
				default: $demtxt="PTE";
								 $class="";	
				break;
			}
			$html .="<li data-role='list-divider'>$demtxt</li>";
			$current_dem=$player->demarcacion;
		}
		$html .="<li><a href='#' data-rel='dialog' id='$player->id' class='infoplayer'>$nombre<span class='ui-li-count'>$statustxt</span></a></li>";
	}
	$html .="</ul>";
	
	return $html;
}

function get_lfpteams_collapsible(){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `".get_pref()."_equiposlfp` ORDER BY `nombre`";
	$obj_teams=$oBBDD->get_resource($sql);
	$html="";
	while ($team=mysqli_fetch_object($obj_teams)){
		$teamname=$team->nombre;
		$list_players=get_players4teams_mobile($team->codigo);
		$html .=<<<eof
		<div id="idlfpteam_{$team->id}" class="summary_lfpteam">
			<h1>{$teamname}</h1>
			{$list_players}
		</div>	
eof;

	}
	return $html;
}

function get_info_player($idplayer){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT jug.id id, jug.nombre nombre, jug.idprop idprop, jug.demarcacion dem,jug.coste coste,jug.equipolfp cod, lfp.nombre equipo, equip.nombre nomprop ";
	$sql .="FROM `".get_pref()."_jugadores` jug INNER JOIN `".get_pref()."_equiposlfp` lfp ON (lfp.codigo=jug.equipolfp)  INNER JOIN `".get_pref()."_equipos` equip ON (jug.idprop=equip.id) WHERE jug.id=".$idplayer;
	$obj_player=$oBBDD->get_resource($sql);
	$player=mysqli_fetch_object($obj_player);
	$playername=$player->nombre;
	$lfpteam=$player->equipo;
	$nomprop=$player->nomprop;
	$cost=$player->coste==0?"":$player->coste;
	
	//miramos a ver si este jugador tiene derechos de formación
	$sql="SELECT count(*) hay FROM `".get_pref()."_formacion` WHERE `idjugador`=".$idplayer;
	$obj_extra=$oBBDD->get_resource($sql);
	$extra=mysqli_fetch_object($obj_extra);
	$asterisk=$extra->hay==0?"":"<span class='formacion' title='jugador con derechos de formación'>*</span>";
	
	switch ($player->dem) {
		case '1': $demtxt="Portero";
		break;
		case '2': $demtxt="Defensa";
		break;
		case '3': $demtxt="Centrocampista";
		break;
		case '4': $demtxt="Delantero";
		break;
		default: $demtxt="Pendiente";
		break;
	}
	
	if ($player->idprop==0)
		$url_escudo="images/escudos/$player->cod.png";
	else
		$url_escudo="images/fichado.png";
		
	$html=<<<eof
		<div id="escudo" class="pretty"><img src="{$url_escudo}"/></div>
		<div id="infoplayer_header" class="pretty" data-role="header"><h1>{$playername}{$asterisk}</h1></div>
		<ul data-role="listview">
			<li><strong>Demarcación: </strong>{$demtxt}</li>
			<li><strong>Equipo LFP: </strong>{$lfpteam}</li>
			<li data-role="list-divider">info subasta</li>
			<li><strong>Propietario: </strong>{$nomprop}</li>
			<li><strong>Coste: </strong>{$cost}</li>
		</ul>
eof;

	return $html;
}

function get_report_team($id){
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT * FROM `".get_pref()."_jugadores` WHERE `idprop`=$id ORDER BY `demarcacion`,`nombre`";

	$obj_players=$oBBDD->get_resource($sql);
	$html=<<<eof
	<table data-role="table" data-mode="columntoggle" class="ui-responsive" id="table_reportteam" data-column-btn-text="columnas">
	<thead>
		<tr>
			<th data-priority="3">Nº</th>
			<th>jugador</th>
			<th data-priority="1">demarcación</th>
			<th data-priority="2">lfp</th>
			<th>coste</th>
		</tr>
	</thead>
	<tbody>
eof;
	$count=1;
	while ($player=mysqli_fetch_object($obj_players)){
		$nombre=$player->nombre;
		switch ($player->demarcacion) {
			case '1': $demtxt="POR";
			break;
			case '2': $demtxt="DEF";
			break;
			case '3': $demtxt="MED";
			break;
			case '4': $demtxt="DEL";
			break;
			default: $demtxt="??";
			break;
		}
		$html .=<<<eof
			<tr>
				<td>{$count}</td>
				<td>{$nombre}</td>
				<td>{$demtxt}</td>
				<td>{$player->equipolfp}</td>
				<td>{$player->coste}</td>
			</tr>
eof;
		$count ++;
	}
	$html .=<<<eof
		</tbody>
  </table>
	<div class="pretty">
		<a href="includes/reports.inc.php?opt=1&idequipo=$id" target="_blank" class="ui-btn ui-btn-inline ui-btn-icon-left ui-icon-action ui-corner-all ui-shadow">exportar a pdf</a>
	</div>
eof;
		
	return $html;
}

function get_full_log_mobile(){
	
	$oBBDD=BBDD::get_instancia();
	$sql="SELECT logg.fecha, logg.tipo, logg.coste, equip.nombre nombreequipo, jug.nombre jugador FROM `".get_pref()."_log` logg ";
	$sql .="INNER JOIN `".get_pref()."_equipos` equip ON (logg.idprop=equip.id) INNER JOIN `".get_pref()."_jugadores` jug ON (jug.id=logg.idplayer) ";
	$sql .="ORDER BY logg.id DESC";
	$obj_log=$oBBDD->get_resource($sql);

	$html=<<<eof
	<div class="prettyright">
	<a href="includes/reports.inc.php?opt=3" target="_blank" class="ui-btn ui-btn-inline ui-btn-icon-left ui-icon-action ui-corner-all ui-shadow">exportar a csv</a>
	<div>
	<table data-role="table" class="ui-responsive" id="table_log">
	<thead>
		<tr>
			<th>hora</th>
			<th>equipo</th>
			<th>jugador</th>
			<th>coste</th>
		</tr>
	</thead>
	<tbody>
eof;
	while ($log=mysqli_fetch_object($obj_log)){
		$equipo=$log->nombreequipo;
		$jugador=$log->jugador;
		$html .=<<<eof
			<tr>
				<td>{$log->fecha}</td>
				<td>{$equipo}</td>
				<td>{$jugador}</td>
				<td>{$log->coste}</td>
			</tr>
eof;
	}
	$html .=<<<eof
		</tbody>
  </table>
eof;
	echo $html;
}

?>

