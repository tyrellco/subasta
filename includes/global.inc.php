<?php
/*
 *      global.inc.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
 
require_once(dirname(__FILE__)."/../classes/BBDD.class.php");

function get_fecha_actual(){
	return date("d\/m\/Y\ -\ H:i:s");
}

function set_new_user($username){
	$_SESSION['subastador']=$username;
	$_SESSION['last']=date("H:i:s");
	
	return $_SESSION['last'];
}

function infouser(){
	if (!isset($_SESSION["subastador"]))
		$_SESSION['subastador']="jaime";
		
	if (!isset($_SESSION['last']))
		$_SESSION['last']=date("H:i:s");
				
	$html=<<<eof
	<div id="infouser">
	<ul>
			<li id="username" title="subastador">{$_SESSION['subastador']}</li>
			<li id="last" title="hora de inicio">{$_SESSION['last']}</li>
			<li id="logout" title="salir y desbloquear">salir</li>
	</ul>
	</div>	
eof;

echo $html;
}

function infouser_ro(){
	$html=<<<eof
	<div id="infouser" class="desktop">
	<ul class="nonetbook">
			<li id="username" title="usuario">esp&iacute;a</li>
			<li id="reload" title="actualizar">actualizar</li>
			<li id="back" title="volver">volver</li>
	</ul>
	</div>	
eof;

echo $html;
}

function infouser_netbook(){
	$html=<<<eof
	<div id="infouser" class="desktop">
	<ul class="netbook">
			<li id="username" title="usuario">esp&iacute;a</li>
			<li id="reload" title="actualizar">actualizar</li>
			<li id="back" title="volver">volver</li>
	</ul>
	</div>	
eof;

echo $html;
}


function about(){
	$anio=YEAR;
	$appname=APPNAME;
	$html=<<<eof
	<div id="about">::: $appname $anio :::</div>
eof;

echo $html;
}

function footer(){
	$versiontxt=APPNAME." ".APPVERSION;
	$html=<<<eof
	<div id="exports">
		<span class='exportpdf' title='generar pdf completo de la subasta'>exportar subasta</span> | 
		<span class='exportcsv' title='export log a fichero csv'>exportar log</span>
	</div>
	<div id="appversion">{$versiontxt}</div>
eof;

echo $html;
}

function passornotpass(){
	if (READONLY){
		$msg="<br/><br/><br/><br/><h1><center>La aplicaci&oacute;n se encuentra en modo de s&oacute;lo lectura<br/></h1>";
		die($msg);
	}	
	else{
	if (file_exists(dirname(__FILE__)."/../run/lock")){
		renew_lock();
		if (!file_exists(dirname(__FILE__)."/../run/lock_".$_SERVER['REMOTE_ADDR'])){ //bloqueado por otro usuario desde otra ip 
			$msg="<br/><br/><br/><br/><h1><center>Ya hay algui&eacute;n m&aacute;s usando la aplicaci&oacute;n,<br/> ";
			$msg .="para evitar problemas no est&aacute;n permitidas las conexiones simult&aacute;neas.</center></h1>";
			die($msg);
		}	
	}
	else
		set_lock();
	}	
}

function renew_lock(){
	$file=fopen(dirname(__FILE__)."/../run/lock", "r");
	if ($file) {
    $timeold = fgets($file, 1024);
    fclose($file);
    $last=time()-$timeold;
    //si ha pasado más de una hora se considera caducado el bloqueo, se vuelve a generar el bloqueo
    if ($last > 3600) 
    	set_lock();
	}
}

function set_lock(){
	//Primero eliminamos cualquier fichero de bloqueo
	$dirmodel=dirname(__FILE__)."/../run/";
	if (is_dir($dirmodel)) {
    if ($handler = opendir($dirmodel)) {
			while (($file = readdir($handler)) !== false) {
				if (is_file($dirmodel.$file)){
						unlink($dirmodel.$file);
					}	
			}
			closedir($handler);
    }
	}
	//Establecemos de nuevo el bloqueo
	$fp = fopen(dirname(__FILE__)."/../run/lock","w+");
	fwrite($fp, time());
	fclose($fp);
	$fp = fopen(dirname(__FILE__)."/../run/lock_".$_SERVER['REMOTE_ADDR'],"w+");
	fwrite($fp, $_SERVER['REMOTE_ADDR']);
	fclose($fp);
}

function byebye(){
	$dirmodel=dirname(__FILE__)."/../run/";
	if (is_dir($dirmodel)) {
    if ($handler = opendir($dirmodel)) {
			while (($file = readdir($handler)) !== false) {
				if (is_file($dirmodel.$file)){
						unlink($dirmodel.$file);
					}	
			}
			closedir($handler);
    }
	}
	unset($_SESSION["year"]);
	return 0;
}

//Pendiente de implementacion
function make_backup(){
	return 0;
	//Dependiendo de si el servidor es UNIX/Linux o Windows las rutas serán de una forma u otra
	/*if (strpos($_SERVER['SERVER_SOFTWARE'], "Unix"))
		$dirbck=dirname(__FILE__)."/../backup/"; //Unix
	else
		$dirbck=mysqli_real_escape_string(dirname(__FILE__)."\..\backup\\"); //Windows
	
	$momento=date("Ymd-His");
	$oBBDD=BBDD::get_instancia();
	$sql="SHOW TABLES	FROM subasta";
	$obj_tables=$oBBDD->get_resource($sql);	
	while($vtable = mysqli_fetch_array($obj_tables)){
		$sqlbackup="SELECT * FROM $vtable[0] INTO OUTFILE '$dirbck$vtable[0]-$momento.csv' FIELDS TERMINATED BY '#' ESCAPED BY '\\\'";
		$oBBDD->set_resultados($sqlbackup);
	}*/
}

function get_css(){
	if (isset($_COOKIE["defaultcss"])){ //si hay cookie le ponemos la css almacenada en la cookie
		$defaultcss=$_COOKIE["defaultcss"];
		$linkcss=<<<eof
		<link id="jquerycss" href="css/jquery-ui-1.8.14.custom-{$defaultcss}.css" rel="stylesheet" type="text/css"/>
		<link id="sitecss" href="css/site-{$defaultcss}.css" rel="stylesheet" type="text/css"/>
		<link id="jqplotcss" href="css/jquery-jqplot-{$defaultcss}.css" rel="stylesheet"  type="text/css"/>
eof;
	}
	else{ //no hay cookie le pongo por defecto la establecida por la aplicación
		$defaultcss=DEFAULTCSS;
		$linkcss=<<<eof
		<link id="jquerycss" href="css/jquery-ui-1.8.14.custom-{$defaultcss}.css" rel="stylesheet" type="text/css"/>
		<link id="sitecss" href="css/site-{$defaultcss}.css" rel="stylesheet" type="text/css"/>
		<link id="jqplotcss" href="css/jquery-jqplot-{$defaultcss}.css" rel="stylesheet"  type="text/css"/>
eof;
	}
	echo $linkcss;
}

function get_pref(){
	if (isset($_SESSION["year"]))
		return $_SESSION["year"];
	else
		return PREF;	
}

function get_year(){
   if (isset($_SESSION["year"]))
      return $_SESSION["year"];
   else
      return YEAR;
}

function set_year($year){
	$_SESSION["year"]=$year;
	return $year;
}
?>
 
