<?php

/*  m.desktop.php
 *
 *  Copyright (C) 2014  Mario Rubiales Gómez <mariorubiales@gmail.com>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
session_save_path(dirname(__FILE__)."/tmp");
session_start();
require_once (dirname(__FILE__)."/includes/m.generator.inc.php");
?>
 
<!DOCTYPE html>
<html>
<head>
	<title>subasta-antiliga</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/mobile/antiliga-mobile.min.css"/>
	<link rel="stylesheet" href="css/mobile/jquery.mobile.icons.min.css"/>
	<link rel="stylesheet" href="css/mobile/jquery.mobile.structure-1.4.3.min.css"/>
	<link rel="stylesheet" href="css/m.site.css"/>
	<script src="libjs/mobile/jquery-1.11.1.min.js"></script>
  <script src="libjs/mobile/jquery.mobile-1.4.3.min.js"></script>
	<script src="js/m.show.js"></script>
	<script>
		/*$(document).delegate("#global_summary", "pagecreate", function() {
			do_get_global_summary();
		});
		$(document).delegate("#summary", "pagecreate", function() {
			do_get_summary_mobile();
		});
		$(document).delegate("#lfpteams", "pagecreate", function() {
			do_get_lfpteams_mobile();
		});
		$(document).delegate("#log", "pagecreate", function() {
			do_get_log_mobile();
		});*/
		/*
			El funcionamiento de los enlaces lo haremos con el evento click y posteriormente
			después de la llamada al ajax mostraremos la página en cuestión y le inyectaremos
			el HTML. 
			
			Hemos descartado hacerlo con de la forma que está comentada más arriba pq no tenía
			forma de evitar la cache de jquery mobile que impedía que la página principal se cargará
			en cada visita y de esta el ajax de consulta a los PHP sólo se ejecutaba la primera vez, 
			por lo que era necesario refrescar las páginas para actualizar los datos.
		*/
		
		$(document).on('pagecreate',function () {
			// eliminamos el evento click para crearlo seguidamente, de esta forma evitamos
			// que se cree el evento en cada carga de cualquier página.
			$("a#link_global_summary, a#link_summary, a#link_lfpteams, a#link_log").unbind("click");
			
			$("a#link_global_summary").bind("click", function(){
				do_get_global_summary();
			});
			
			$("a#link_summary").bind( "click", function() {
				do_get_summary_mobile();
			});
			
			$("a#link_lfpteams").bind( "click", function() {
				do_get_lfpteams_mobile();
			});
			
			$("a#link_log").bind( "click", function() {
				do_get_log_mobile();
			});
		});	

</script>	
</head>
<body>
<!-- HOME -->
<div data-role="page" id="home">
	<div role="content" class="ui-content">
		<div class="pretty" id="logoapp">
			<img src="images/logo_mobile.png" />
		</div>
	<div id="main_menu">
			<a href="#" id="link_global_summary" class="ui-btn ui-btn-icon-left ui-icon-eye ui-corner-all ui-shadow">visión general</a>
			<a href="#" id="link_summary" class="ui-btn ui-icon-grid ui-btn-icon-left ui-corner-all ui-shadow">info subasta</a>
			<a href="#" id="link_lfpteams" class="ui-btn ui-icon-bullets ui-btn-icon-left ui-corner-all ui-shadow">plantillas LFP</a>
			<a href="#" id="link_log" class="ui-btn ui-icon-bars ui-btn-icon-left ui-corner-all ui-shadow">mostrar log</a>
		</div>
	</div>
	<div data-role="footer" id="footerhome">
		<small>subasta-antiliga-mobile v0.1</small>
	</div>
</div>
<!-- /HOME -->

<!-- GLOBAL_SUMMARY -->
<div data-role="page" id="global_summary">
   <div data-role="header">
		<h1>visión general</h1>
		<a href="#home" data-icon="home" data-iconpos="notext" title="inicio" data-ajax="false">Home</a>
	</div><!-- /header -->
   <div data-role="content" class="ui-content">   
      <div id="content_global_summary" class="ui-content">
		</div>
   </div>
   <div data-role="footer">
   </div>
</div>
<!-- /GLOBAL_SUMMARY -->

<!-- SUMMARY -->
<div data-role="page" id="summary">
   <div data-role="header">
		<h1>resumen subasta</h1>
		<a href="#home" data-icon="home" data-iconpos="notext" data-ajax="false" title="inicio">Home</a>
	</div><!-- /header -->
   <div data-role="content" class="ui-content">   
      <div id="content_summary" class="ui-content">
			</div>
   </div>
   <div data-role="footer">
   </div>
</div>
<!-- /SUMMARY -->

<!-- LFPTEAMS -->
<div data-role="page" id="lfpteams">
   <div data-role="header">
		<h1>plantillas LFP</h1>
		<a href="#home" data-icon="home" data-iconpos="notext" data-ajax="false" title="inicio">Home</a>
	</div><!-- /header -->
   <div data-role="content" class="ui-content">   
      <div id="content_lfpteams" class="ui-content">
		</div>
   </div>
   <div data-role="footer">
   </div>
</div>
<!-- /LFPTEAMS -->

<!-- INFOPLAYER -->
<div data-role="page" id="infoplayer">
   <div data-role="header">
		<h1>info jugador</h1>
		<a href="#" data-iconpos="notext" data-icon="delete" data-corners="true" data-rel="back" title="cerrar">
		</a>
	</div><!-- /header -->
   <div data-role="content" class="ui-content">   
      <div id="content_infoplayer" class="ui-content">
		</div>
   </div>
   <div data-role="footer">
   </div>
</div>
<!-- /INFOPLAYER -->

<!-- REPORTTEAM -->
<div data-role="page" id="reportteam">
   <div data-role="header">
		<h1>plantillas antiliga</h1>
		<a href="#" data-icon="back" data-iconpos="notext" data-ajax="false" title="volver" data-rel="back">volver</a>
	</div><!-- /header -->
   <div data-role="content" class="ui-content">   
      <div id="content_reportteam" class="ui-content">
		</div>
   </div>
   <div data-role="footer">
   </div>
</div>
<!-- /REPORTTEAM -->

<!-- LOG -->
<div data-role="page" id="log">
   <div data-role="header">
		<h1>log subasta</h1>
		<a href="#home" data-icon="home" data-iconpos="notext" data-ajax="false" title="inicio">Home</a>
	</div><!-- /header -->
   <div data-role="content" class="ui-content">   
      <div id="content_log" class="ui-content">
			</div>
   </div>
   <div data-role="footer">
   </div>
</div>
<!-- /LOG -->
</body>
</html>
