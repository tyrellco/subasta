<?php
/*
 *      BBDD.class.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

 
/* Esta clase realiza cualquier tipo de operación sobre una BBDD MySQL 
 * Clase desarrollada siguiendo el patrón singleton
 */

require_once (dirname(__FILE__)."/../conf/config.php");

class BBDD {

	private static $instancia;
	private $server=SERVER;
	private $bbddname=BBDD;
	private $bbdduser=USER;
	private $bbddpasswd=PASSWD;
	private $link;
	private $resultado;
	private $error;
	private $txterror;

/*
 * Constructor singleton de la clase, es privado para que no se pueda instanciar desde fuera.
 * Realiza una conexión con la BBDD.
 * @return objeto-conexión con la bbdd.
 */
	private function __construct(){
		$this->link=@mysqli_connect($this->server,$this->bbdduser,$this->bbddpasswd);
		if (!$this->link){
			die("<br/>Error de conexión con la bbdd:".mysqli_error($this->link));
		}
		if (!@mysqli_select_db($this->link, $this->bbddname)){
			die("<br/>Error al seleccionar la bbdd:".mysqli_error($this->link));
		}
		return $this->link;
	}
/*
 * Método que retorna una instancia de la BBDD, la instancia sólo se creará la primera vez que se invoque
 * a dicho método, el resto de veces restornará siempre la misma instancia.
 * @return instancia de la propia clase.
 */
	public static function get_instancia(){
		if (self::$instancia===null)
			self::$instancia=new BBDD();
		return self::$instancia;
	}

	/*
 * Método que realiza una consulta cualquiera contra la BBDD
 * @return objeto-resultado de la consulta.
 */

 	public function get_resource($sql){
		$this->resultado=@mysqli_query($this->link, $sql);
		if (!$this->resultado)
			die ("<br/>Error al ejecutar la consulta en la BBDD:".$sql."<br/><b>".mysqli_error($this->link)."</b>");
		return $this->resultado;
	}
	
/*
 * Método que realiza una consulta de actualización contra la BBDD,
 * especialemente pensado para los INSERT y UPDATE.
 * @sql Literal de la consulta SQL
 */

//Produccion
	public function set_resultados($sql){
		if (!READONLY){
			$this->resultado=@mysqli_query($this->link, $sql);
			if (!$this->resultado)
				die ("<br/>Error al ejecutar actualización y/o inserción en la BBDD: ".$sql."<br/><b>".mysqli_error($this->link)."</b>");
		}
		else
			die("Aplicación sólo en modo lectura");	
	}
	
/*
 * Sobrecarga del método clone para impedir que clonen una clase singleton.
 */
	public function __clone(){
		trigger_error("ERROR: No te voy a permitir clonar una clase singleton",E_USER_ERROR);
	}
}

?>
