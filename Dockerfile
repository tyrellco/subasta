FROM php:7.0.33-apache-jessie
RUN docker-php-ext-install mysqli
RUN cp -f /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
COPY 000-default.conf /etc/apache2/sites-available/