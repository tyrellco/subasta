<?php
/*
 *      desktop-netbook.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/tmp");
session_start();
require_once (dirname(__FILE__)."/includes/generator.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>subasta-antiliga</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
<?php get_css(); ?>
<script language="Javascript" src="includes/js.php?opt=1" type="text/javascript"></script>
<script language="Javascript" src="libjs/jquery-1.6.2.min.js" type="text/javascript"></script>
<script language="Javascript" src="libjs/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
<script language="Javascript" src="libjs/jquery.cookie.js" type="text/javascript"></script>
<script language="Javascript" src="js/global.js" type="text/javascript"></script>
<script language="Javascript" src="js/show.js" type="text/javascript"></script>
<!-- librerias jqplot -->
<script language="javascript" type="text/javascript" src="libjs/jquery.jqplot.js"></script>
<script language="javascript" type="text/javascript" src="libjs/plugins/jqplot.barRenderer.js"></script>
<script language="javascript" type="text/javascript" src="libjs/plugins/jqplot.pieRenderer.js"></script>
<script language="javascript" type="text/javascript" src="libjs/plugins/jqplot.categoryAxisRenderer.js"></script>
<script language="javascript" type="text/javascript" src="libjs/plugins/jqplot.highlighter.js"></script>
<script language="javascript" type="text/javascript" src="libjs/plugins/jqplot.pointLabels.js"></script>
<!------------------------>

<script language="Javascript" type="text/javascript">
	$(document).ready(function(){
		init_app();
		do_launch_graph();
});
</script>
</head>
<body class="desktop">
<div id="report_popup" class="netbook"></div>
<div id="header">
	<?php about(); infouser_netbook(); ?>
</div>
<div id="main">
	<div id="working" class="desktop">
		<div id="summary" class="desktop">
			<div class="label">:: resumen ::</div>
			<div class="content">
				<?php get_full_summary();?>
			</div>
		</div>	
		<div id="searcher">
			<div class="label">:: buscador ::</div>
			<div class="content">
				<div id="inputsearch">
					<input type="text" id="playername_ro" name="playername_ro" />
				</div>
			</div>
		</div>
		<div id="record">
			<div class="label">:: gr&aacute;fica dinero restante ::</div>
			<div class="content">
				<div id="graph"></div>
			</div>
		</div>
		<div id="lfpteams">
			<div class="label">:: equipos LFP</div>
			<div class="content"><?php echo get_lfpteams_desktop() ?> </div>
		</div>
		<div id="players4teams">
			<div class="label">:: jugadores ::</div>
			<div class="content"></div>
		</div>
	</div>	
</div>
<div id="footer"><?php echo footer()?></div>
</body>
</html>
