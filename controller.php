<?php
/*
 *      controller.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

session_save_path(dirname(__FILE__)."/tmp");
session_start();
require_once (dirname(__FILE__)."/includes/generator.inc.php");
passornotpass();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>subasta-antiliga</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<?php get_css(); ?>
<script language="Javascript" src="includes/js.php?opt=1" type="text/javascript"></script>
<script language="Javascript" src="libjs/jquery-1.6.2.min.js" type="text/javascript"></script>
<script language="Javascript" src="libjs/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
<script language="Javascript" src="libjs/jquery.cookie.js" type="text/javascript"></script>
<script language="Javascript" src="js/global.js" type="text/javascript"></script>
<script language="Javascript" src="js/show.js" type="text/javascript"></script>
<script language="Javascript" type="text/javascript">
	$(document).ready(function(){
		init_app();
	});
</script>
</head>
<body>
<div id="report_popup"></div>
<div id="bck_popup">haciendo copia de seguridad, espera un momento...</div>
<div id="header">
	<?php about(); infouser(); ?>
</div>
<div id="main">
	<div id="working">
		<div id="summary">
			<div class="label" title='generar informe completo de la subasta'>:: resumen ::</div>
			<div class="content">
				<?php get_full_summary();?>
			</div>
		</div>	
		<div id="searcher">
			<div class="label">:: buscador ::</div>
			<div class="content">
				<div id="inputsearch">
					<input type="text" id="playername" name="playername" />
				</div>
				<div id="inputnew">
					<input type="button" id="btnnew" name="btnnew" class="accept" value="crear nuevo jugador"/>
				</div>
			</div>
		</div>
		<div id="record">
			<div class="label">:: datos jugador ::</div>
			<div class="content"></div>
		</div>
		<div id="lfpteams">
			<div class="label">:: equipos LFP</div>
			<div class="content"><?php echo get_lfpteams() ?> </div>
		</div>
		<div id="players4teams">
			<div class="label">:: jugadores ::</div>
			<div class="content"></div>
		</div>
	</div>	
	<div id="log">
		<div class="label" title='exportar log a fichero CSV'>:: log ::</div>
		<div class="content"><?php get_full_log(0);?></div>
	</div>
	<div id="labelcount">NÚMERO DE FICHAJES: <span id="count"><?php echo get_count_transfers();?></span></div>
</div>
<div id="footer"><?php echo footer()?></div>
</body>
</html>
