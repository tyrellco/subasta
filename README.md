# subasta

## Actualización (7 Julio, 2016)

Aplicación web para la gestión y administración de la subasta anual de la [antiliga](http://antiliga.org/?page_id=2 "antiliga")

## Tecnologías empleadas
+ PHP
+ Javascript
+ jQuery
+ jQuery-mobile
+ CSS

## Requerimientos

Esta aplicacón ha sido implementada para funcionar bajo un entorno LAMP/WAMP/MAMP, esto es:
+ Servidor HTTP con soporte para PHP (Apache, Nginx, etc)
+ Servidor de BBDD MySQL
+ Linux, Windows o Mac

## Instalación

1. Importar el esquema de la BBDD que se encuentra dentro del directorio <code>bbdd</code> en nuestro servidor MySQL
2. Subir el código al servidor web siguiendo las instrucciones del servidor seleccionado.

## Licencia:

Esta aplicación está diseñada especificamente para la [antiliga](http://antiliga.org/?page_id=2 "antiliga") lo que significa que su uso no tiene sentido fuera de este juego. No obstante, nuestra aplicación está licenciada bajo licencia [GPLv3+] (http://www.gnu.org/licenses/gpl-3.0.html "GPLv3+") para todo aquel que puediera estar interesado, bien para crear nuevas comunidades de la [antiliga](http://antiliga.org/?page_id=2 "antiliga") o bien para reutilizar partes del código, creación de forks, etc.